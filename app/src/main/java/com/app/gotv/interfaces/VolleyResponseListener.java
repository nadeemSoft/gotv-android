package com.app.gotv.interfaces;

/**
 * Created by codezilla-8 on 20/4/18.
 */

public interface VolleyResponseListener {
    void onError(String message);

    void onResponse(Object response);


}
