package com.app.gotv.model;

import android.os.Parcel;
import android.os.Parcelable;


public class AllCitizenModel implements Parcelable {
    String _id;
    String polling_station;
    String voter_id;
    String name;
    int age;
    String sex;
    String updatedAt;
    String createdAt;

    protected AllCitizenModel(Parcel in) {
        _id = in.readString();
        polling_station = in.readString();
        voter_id = in.readString();
        name = in.readString();
        age = in.readInt();
        sex = in.readString();
        updatedAt = in.readString();
        createdAt = in.readString();
    }

    public static final Creator<AllCitizenModel> CREATOR = new Creator<AllCitizenModel>() {
        @Override
        public AllCitizenModel createFromParcel(Parcel in) {
            return new AllCitizenModel(in);
        }

        @Override
        public AllCitizenModel[] newArray(int size) {
            return new AllCitizenModel[size];
        }
    };

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPolling_station() {
        return polling_station;
    }

    public void setPolling_station(String polling_station) {
        this.polling_station = polling_station;
    }

    public String getVoter_id() {
        return voter_id;
    }

    public void setVoter_id(String voter_id) {
        this.voter_id = voter_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeString(polling_station);
        dest.writeString(voter_id);
        dest.writeString(name);
        dest.writeInt(age);
        dest.writeString(sex);
        dest.writeString(updatedAt);
        dest.writeString(createdAt);
    }
}
