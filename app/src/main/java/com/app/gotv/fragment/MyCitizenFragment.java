package com.app.gotv.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.gotv.R;
import com.app.gotv.abstractPackage.EndlessParentScrollListener;
import com.app.gotv.activity.AddCitizenActivity;
import com.app.gotv.activity.HomeActivity;
import com.app.gotv.activity.SplashActivity;
import com.app.gotv.adapter.HomeAdapter2;
import com.app.gotv.adapter.MyCitizenAdapter;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.model.AllCitizenModel;
import com.app.gotv.sessionManagement.AppSession;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.vollysinglton.VolleyUtils;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class MyCitizenFragment extends Fragment implements View.OnClickListener {
    private TextView tvToolbar = null, tvBatch = null, tvNoCitizen = null;
    private Context activity = null;
    private ImageView ivOptionMenu = null;
    private RecyclerView rvHome = null;
    private Dialog progressDialog = null;
    private AppSession appSession = null;
    private CircleImageView circleImageView = null;
    private NestedScrollView nestedScrollView = null;
    private JSONArray jsonArrayResponse = null;
    private ArrayList<AllCitizenModel> modelArrayList = null;
    private Gson gson;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getContext();
        appSession = AppSession.getInstance(activity);
        modelArrayList = new ArrayList<>();
        gson = new Gson();

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initWidgets(view);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        tvToolbar.setText("" + appSession.getMyName());
        setUserImage(appSession.getUserImage());


        return view;
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();

        getAllCitizen();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_option_menu:
                showPopupMenu(v);
                break;
           /* case R.id.fab:

                if (jsonArrayResponse != null) {

                    if (goldValue >= 5) {
                        AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.not_Add_citizen));


                    } else {
                        Intent intent = new Intent(activity, AddCitizenActivity.class);
                        activity.startActivity(intent);

                    }


                } else {
                    Intent intent = new Intent(activity, AddCitizenActivity.class);
                    activity.startActivity(intent);
                }


                break;*/
        }

    }

    private void initWidgets(View view) {
        tvToolbar = view.findViewById(R.id.tv_toolbar);
        ivOptionMenu = view.findViewById(R.id.iv_option_menu);
        ivOptionMenu.setVisibility(View.VISIBLE);
        rvHome = view.findViewById(R.id.rv_home);
        circleImageView = view.findViewById(R.id.iv_profile);
        tvBatch = view.findViewById(R.id.tv_bach);
        tvNoCitizen = view.findViewById(R.id.tv_no_citizen);
        nestedScrollView = view.findViewById(R.id.nestedScroll);
        ivOptionMenu.setOnClickListener(this);
        progressDialog = AppUtil.showLoaderDialog(activity, false);


    }

    private void setUserImage(String userImage) {

       /* Picasso.get()
                .load(userImage)
                .resize(100, 100)
                .centerCrop()
                .into(circleImageView);*/


        Glide.with(getActivity())
                .load(userImage)
                .placeholder(R.mipmap.app_icon)
                .into(circleImageView);
       // new ImageTask().execute(userImage);

    }


    public void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(activity, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.bottom_nav_menu, popup.getMenu());


        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                int id = item.getItemId();

                switch (id) {
                    case R.id.logout:
                        showLogoutDialog(activity);
                        return true;
                }
                return false;
            }
        });
        popup.show();
    }


    private void showLogoutDialog(final Context activity) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.common_dialog);
        //dialog.setContentView(R.layout.common_dialog);
        // Window window = dialog.getWindow();
        // window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        TextView tvSms = dialog.findViewById(R.id.popup_content);
        TextView btnNo = dialog.findViewById(R.id.popup_no_btn);
        TextView btnYes = dialog.findViewById(R.id.popup_yes_btn);

        tvSms.setText("" + getResources().getString(R.string.logout_app));

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isLogout = appSession.logoutUser();
                if (isLogout) {
                    Intent intent = new Intent(activity, SplashActivity.class);
                    activity.startActivity(intent);
                    ((Activity) activity).finishAffinity();


                } else {
                    Toast.makeText(activity, "User is not logout", Toast.LENGTH_SHORT).show();

                }
                dialog.cancel();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();

            }
        });


        dialog.show();


    }


    private void getAllCitizen() {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_CITIZEN;
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                progressDialog.dismiss();
                Log.e("MyCitizenData", response + "");

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {

                        jsonArrayResponse = jsonObject.optJSONArray(KeyConstant.KEY_RESPONSE);

                        if (jsonArrayResponse != null) {
                            Log.e("length", jsonArrayResponse.length() + "");


                            if (jsonArrayResponse.length() == 0) {
                                String sms = activity.getResources().getText(R.string.no_citizen).toString();
                                tvNoCitizen.setText(sms);
                                rvHome.setVisibility(View.GONE);
                                tvNoCitizen.setVisibility(View.VISIBLE);
                            } else if (jsonArrayResponse.length() > 0) {
                                rvHome.setVisibility(View.VISIBLE);
                                tvNoCitizen.setVisibility(View.GONE);
                            }




                           /* for (int i = 0; i < jsonArrayResponse.length(); i++) {
                                JSONObject object = jsonArrayResponse.optJSONObject(i);

                                boolean isMain = object.optBoolean("isMain");
                                Log.e("isMain",isMain+"");

                                if (isMain) {
                                    HomeActivity.goldValue = HomeActivity.goldValue + 1;
                                }


                            }*/


                            LinearLayoutManager layoutManager = new LinearLayoutManager(activity, RecyclerView.VERTICAL, false);
                            rvHome.setLayoutManager(layoutManager);
                            MyCitizenAdapter adapter = new MyCitizenAdapter(activity, jsonArrayResponse);
                            rvHome.setAdapter(adapter);
                            rvHome.setNestedScrollingEnabled(false);


                        } else {
                            String sms = activity.getResources().getText(R.string.no_citizen).toString();
                            //AppUtil.showCommonPopup(activity, sms);

                            tvNoCitizen.setText(sms);
                            rvHome.setVisibility(View.GONE);
                            tvNoCitizen.setVisibility(View.VISIBLE);

                        }


                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public class ImageTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            URL url = null;
            try {
                url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection)
                        url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            Log.e("BItmap",bitmap+"");
            circleImageView.setImageBitmap(bitmap);
        }
    }


}
