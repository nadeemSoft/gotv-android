package com.app.gotv.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.gotv.R;
import com.app.gotv.abstractPackage.EndlessParentScrollListener;
import com.app.gotv.activity.HomeActivity;
import com.app.gotv.adapter.MyCitizenAdapter;
import com.app.gotv.adapter.MyIncidentAdapter;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.sessionManagement.AppSession;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.vollysinglton.VolleyUtils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyIncidentFragment extends Fragment implements View.OnClickListener {
    private TextView tvToolbar = null, tvBatch = null, tvNoIncident = null;
    private Context activity = null;
    private RecyclerView rvIncident = null;
    private Dialog progressDialog = null;
    private AppSession appSession = null;
    private CircleImageView circleImageView = null;
    private NestedScrollView nestedScrollView = null;
    private JSONArray jsonArrayDocs = null;
    private MyIncidentAdapter incidentAdapter = null;
    private LinearLayoutManager layoutManager = null;


    int page = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_incident, container, false);
        activity = getContext();
        initWidgets(view);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        tvToolbar.setText("" + getContext().getResources().getString(R.string.my_incident));
        setUserImage(appSession.getUserImage());


        layoutManager = new LinearLayoutManager(activity, RecyclerView.VERTICAL, false);
        rvIncident.setLayoutManager(layoutManager);
        rvIncident.setHasFixedSize(false);


        nestedScrollView.setOnScrollChangeListener(new EndlessParentScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int pag, int totalItemsCount) {
                page++;
                getMyIncidentLoadMore(page);

            }
        });


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        page = 1;
        getMyIncident(page);

    }

    @Override
    public void onClick(View v) {

    }


    private void initWidgets(View view) {
        tvToolbar = view.findViewById(R.id.tv_toolbar);
        rvIncident = view.findViewById(R.id.rv_incident);
        circleImageView = view.findViewById(R.id.iv_profile);
        tvBatch = view.findViewById(R.id.tv_bach);
        tvNoIncident = view.findViewById(R.id.tv_no_incident);
        nestedScrollView = view.findViewById(R.id.nestedScroll);
        appSession = AppSession.getInstance(activity);
        progressDialog = AppUtil.showLoaderDialog(activity, false);


    }


    private void setUserImage(String userImage) {

     /*   Picasso.get()
                .load(userImage)
                .resize(100, 100)
                .centerCrop()
                .into(circleImageView);*/

        Glide.with(getActivity())
                .load(userImage)
                .placeholder(R.mipmap.app_icon)
                .into(circleImageView);

        //new ImageTask().execute(userImage);


    }






    private void getMyIncident(int page) {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_MY_CITIZEN + "?page=" + page + "&limit=10";
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                Log.e("MyIncidentData", response + "");

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {
                        progressDialog.dismiss();

                        JSONObject objectResponse = jsonObject.optJSONObject(KeyConstant.KEY_RESPONSE);
                        jsonArrayDocs = objectResponse.optJSONArray(KeyConstant.KEY_DOCS);

                        if (jsonArrayDocs != null) {
                            Log.e("length", jsonArrayDocs.length() + "");


                            if (jsonArrayDocs.length() == 0) {
                                String sms = activity.getResources().getText(R.string.no_incident).toString();
                                tvNoIncident.setText(sms);
                                rvIncident.setVisibility(View.GONE);
                                tvNoIncident.setVisibility(View.VISIBLE);
                            } else if (jsonArrayDocs.length() > 0) {
                                rvIncident.setVisibility(View.VISIBLE);
                                tvNoIncident.setVisibility(View.GONE);


                                incidentAdapter = new MyIncidentAdapter(activity, jsonArrayDocs);
                                rvIncident.setAdapter(incidentAdapter);

                            }


                        } else {
                            String sms = activity.getResources().getText(R.string.no_citizen).toString();
                            //AppUtil.showCommonPopup(activity, sms);

                            tvNoIncident.setText(sms);
                            rvIncident.setVisibility(View.GONE);
                            tvNoIncident.setVisibility(View.VISIBLE);

                        }


                    } else {
                        progressDialog.dismiss();

                        AppUtil.showCommonPopup(activity, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();

                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    private void getMyIncidentLoadMore(int page) {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_MY_CITIZEN + "?page=" + page + "&limit=10";
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                progressDialog.dismiss();
                Log.e("MyIncidentData", response + "");

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {

                        JSONObject objectResponse = jsonObject.optJSONObject(KeyConstant.KEY_RESPONSE);
                        JSONArray jsonArrayResponse = objectResponse.optJSONArray(KeyConstant.KEY_DOCS);

                        if (jsonArrayResponse != null) {
                            Log.e("length", jsonArrayResponse.length() + "");


                            int currentSize = jsonArrayDocs.length();

                            for (int i = 0; i < jsonArrayResponse.length(); i++) {
                                JSONObject object = jsonArrayResponse.optJSONObject(i);

                                jsonArrayDocs.put(object);

                            }
                            if (jsonArrayDocs != null) {
                                incidentAdapter.notifyItemRangeInserted(currentSize, jsonArrayDocs.length());
                            }


                        } else {
                            String sms = activity.getResources().getText(R.string.no_citizen).toString();
                            //AppUtil.showCommonPopup(activity, sms);

                            tvNoIncident.setText(sms);
                            rvIncident.setVisibility(View.GONE);
                            tvNoIncident.setVisibility(View.VISIBLE);

                        }


                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public class ImageTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            URL url = null;
            try {
                url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection)
                        url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            Log.e("BItmap",bitmap+"");
            circleImageView.setImageBitmap(bitmap);
        }
    }

}
