package com.app.gotv.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.gotv.R;
import com.app.gotv.abstractPackage.EndlessParentScrollListener;
import com.app.gotv.activity.AddCitizenActivity;
import com.app.gotv.activity.HomeActivity;
import com.app.gotv.adapter.HomeAdapter;
import com.app.gotv.adapter.HomeAdapter2;
import com.app.gotv.async.ImageTask;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.model.AllCitizenModel;
import com.app.gotv.sessionManagement.AppSession;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.vollysinglton.VolleyUtils;
import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class HomeFragment extends Fragment implements View.OnClickListener {
    private TextView tvToolbar = null, tvBatch = null, tvNoCitizen = null;
    private Context activity = null;
    private ImageView ivSearch = null;
    private RecyclerView rvHome = null;
    private Dialog progressDialog = null;
    private AppSession appSession = null;
    private CircleImageView circleImageView = null;
    private NestedScrollView nestedScrollView = null;
    private JSONArray jsonArrayResponse = null;
    private ArrayList<AllCitizenModel> modelArrayList = null;
    private Gson gson;
    private HomeAdapter2 adapter = null;
    private int page = 0, limit = 10;
    private EditText etGlobalSearch = null;
    private LinearLayout llQuerySearch = null;
    private EndlessParentScrollListener scrollListener;
    private  LinearLayoutManager layoutManager =null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getContext();
        appSession = AppSession.getInstance(activity);
        gson = new Gson();

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        initWidgets(view);

        tvToolbar.setText("" + appSession.getMyName());
        setUserImage(appSession.getUserImage());
        Log.e("IMgae", appSession.getUserImage() + "");
        modelArrayList = new ArrayList<>();


         layoutManager = new LinearLayoutManager(activity, RecyclerView.VERTICAL, false);
        rvHome.setLayoutManager(layoutManager);
        rvHome.setHasFixedSize(false);
        rvHome.setNestedScrollingEnabled(false);


        scrollListener = new EndlessParentScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                page++;
                getAllCitizenWithLoadMore(page);
            }


        };

        nestedScrollView.setOnScrollChangeListener(scrollListener);




        etGlobalSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {

                    AppUtil.hideSoftKeyboard(activity);

                    String data = etGlobalSearch.getText().toString().trim();

                    if (data.length() > 0) {
                        modelArrayList.clear();

                        getAllCitizenWithSearch(data, page);
                        etGlobalSearch.getText().clear();
                    }

                    return true;
                }
                return false;
            }
        });


        etGlobalSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String query = s.toString().toLowerCase().trim();
                getAllCitizenWithTextSorting(query, page);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        page = 1;
        etGlobalSearch.getText().clear();
        getAllCitizen(page);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_search_global:
                if (!etGlobalSearch.getText().toString().trim().equalsIgnoreCase("")) {
                    String data = etGlobalSearch.getText().toString().trim();
                    modelArrayList.clear();

                    getAllCitizenWithSearch(data, page);
                    etGlobalSearch.getText().clear();

                }

                break;
           /* case R.id.fab:

                if (jsonArrayResponse != null) {

                    if (goldValue >= 5) {
                        AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.not_Add_citizen));


                    } else {
                        Intent intent = new Intent(activity, AddCitizenActivity.class);
                        activity.startActivity(intent);

                    }


                } else {
                    Intent intent = new Intent(activity, AddCitizenActivity.class);
                    activity.startActivity(intent);
                }


                break;*/
        }

    }

    private void initWidgets(View view) {
        tvToolbar = view.findViewById(R.id.tv_toolbar);
        rvHome = view.findViewById(R.id.rv_home);
        llQuerySearch = view.findViewById(R.id.ll_query_search);
        llQuerySearch.setVisibility(View.VISIBLE);
        circleImageView = view.findViewById(R.id.iv_profile);
        tvBatch = view.findViewById(R.id.tv_bach);
        tvNoCitizen = view.findViewById(R.id.tv_no_citizen);
        nestedScrollView = view.findViewById(R.id.nestedScroll);
        etGlobalSearch = view.findViewById(R.id.et_global_search);
        ivSearch = view.findViewById(R.id.iv_search_global);
        ivSearch.setOnClickListener(this);
        progressDialog = AppUtil.showLoaderDialog(activity, false);


    }

    private void setUserImage(final String userImage) {


        //  Bitmap image =getBitmapFromURL(userImage);
        // circleImageView.setImageBitmap(image);

     /*   Picasso.get()
                .load(userImage)
                .resize(100, 100)
                .centerCrop()
                .into(circleImageView);*/

         // new ImageTask().execute(userImage);


        Glide.with(getActivity())
                .load(userImage)
                .placeholder(R.mipmap.app_icon)
                .into(circleImageView);


    }






    private void getAllCitizen(int page) {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_CITIZEN_NEW + "?&page=" + page + "&limit=" + limit;
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                progressDialog.dismiss();
                Log.e("AllCitizenHome", response + "");
                modelArrayList.clear();
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {


                        JSONObject objectRes = jsonObject.optJSONObject(KeyConstant.KEY_RESPONSE);

                        jsonArrayResponse = objectRes.optJSONArray(KeyConstant.KEY_DOCS);

                        if (jsonArrayResponse != null) {
                            Log.e("length", jsonArrayResponse.length() + "");


                            if (jsonArrayResponse.length() == 0) {
                                String sms = activity.getResources().getText(R.string.no_citizen).toString();
                                tvNoCitizen.setText(sms);
                                rvHome.setVisibility(View.GONE);
                                tvNoCitizen.setVisibility(View.VISIBLE);
                            } else if (jsonArrayResponse.length() > 0) {
                                rvHome.setVisibility(View.VISIBLE);
                                tvNoCitizen.setVisibility(View.GONE);
                            }


                            for (int i = 0; i < jsonArrayResponse.length(); i++) {
                                JSONObject object = jsonArrayResponse.optJSONObject(i);

                                AllCitizenModel allCitizenModel = gson.fromJson(object.toString(), AllCitizenModel.class);

                                modelArrayList.add(allCitizenModel);


                            }


                            adapter = new HomeAdapter2(activity, modelArrayList);
                            rvHome.setAdapter(adapter);
                            //adapter.notifyDataSetChanged();


                        } else {
                            String sms = activity.getResources().getText(R.string.no_citizen).toString();
                            //AppUtil.showCommonPopup(activity, sms);

                            tvNoCitizen.setText(sms);
                            rvHome.setVisibility(View.GONE);
                            tvNoCitizen.setVisibility(View.VISIBLE);

                        }


                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    private void getAllCitizenWithLoadMore(int page) {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();

        String url = UrlConstant.URL_GET_ALL_CITIZEN_NEW + "?&page=" + page + "&limit=" + limit;

        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                progressDialog.dismiss();
                Log.e("AllCitizenHomeLoad", response + "");

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {


                        JSONObject objectRes = jsonObject.optJSONObject(KeyConstant.KEY_RESPONSE);

                        jsonArrayResponse = objectRes.optJSONArray(KeyConstant.KEY_DOCS);

                        if (jsonArrayResponse != null) {
                            Log.e("length", jsonArrayResponse.length() + "");
                            int currentSize = modelArrayList.size();


                            for (int i = 0; i < jsonArrayResponse.length(); i++) {
                                JSONObject object = jsonArrayResponse.optJSONObject(i);

                                AllCitizenModel allCitizenModel = gson.fromJson(object.toString(), AllCitizenModel.class);

                                modelArrayList.add(allCitizenModel);

                            }
                            if (!modelArrayList.isEmpty()) {

                               // adapter.notifyItemInserted(modelArrayList.size());

                                adapter.notifyItemRangeInserted(currentSize, modelArrayList.size());

                                progressDialog.dismiss();

                            }

                        }


                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    private void getAllCitizenWithSearch(String data, int page) {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_CITIZEN_NEW_SEARCH + "?search=" + data + "&page=" + page + "&limit=" + limit;
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                progressDialog.dismiss();
                Log.e("AllCitizenHome", response + "");

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {

                        JSONObject objectRes = jsonObject.optJSONObject(KeyConstant.KEY_RESPONSE);

                        jsonArrayResponse = objectRes.optJSONArray(KeyConstant.KEY_DOCS);

                        if (jsonArrayResponse != null) {
                            Log.e("length", jsonArrayResponse.length() + "");


                            if (jsonArrayResponse.length() == 0) {
                                Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();

                            } else if (jsonArrayResponse.length() > 0) {

                                for (int i = 0; i < jsonArrayResponse.length(); i++) {
                                    JSONObject object = jsonArrayResponse.optJSONObject(i);

                                    AllCitizenModel allCitizenModel = gson.fromJson(object.toString(), AllCitizenModel.class);

                                    modelArrayList.add(allCitizenModel);


                                }
                                if (!modelArrayList.isEmpty()) {

                                    adapter.notifyItemInserted(modelArrayList.size());

                                    progressDialog.dismiss();

                                }
                            }


                        } else {
                            Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();

                        }


                    } else {
                        Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    private void getAllCitizenWithTextSorting(String data, int page) {

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_CITIZEN_NEW_SEARCH + "?search=" + data + "&page=" + page + "&limit=" + limit;
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                Log.e("AllCitizenSearch", response + "");
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {
                        modelArrayList.clear();
                        scrollListener.resetState();

                        JSONObject objectRes = jsonObject.optJSONObject(KeyConstant.KEY_RESPONSE);

                        jsonArrayResponse = objectRes.optJSONArray(KeyConstant.KEY_DOCS);

                        if (jsonArrayResponse != null) {
                            Log.e("length", jsonArrayResponse.length() + "");


                            if (jsonArrayResponse.length() == 0) {
                                Toast.makeText(activity, "Data Not Found", Toast.LENGTH_SHORT).show();

                            } else if (jsonArrayResponse.length() > 0) {

                                for (int i = 0; i < jsonArrayResponse.length(); i++) {
                                    JSONObject object = jsonArrayResponse.optJSONObject(i);

                                    AllCitizenModel allCitizenModel = gson.fromJson(object.toString(), AllCitizenModel.class);

                                    modelArrayList.add(allCitizenModel);


                                }
                                adapter = new HomeAdapter2(activity, modelArrayList);
                                rvHome.setAdapter(adapter);


                            }


                        } else {
                            Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();

                        }


                    } else {
                        Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    public class ImageTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            URL url = null;
            try {
                url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection)
                        url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            Log.e("BItmap", bitmap + "");
            circleImageView.setImageBitmap(bitmap);
        }
    }


}
