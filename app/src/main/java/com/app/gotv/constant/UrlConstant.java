package com.app.gotv.constant;

public interface UrlConstant {

    int CONNECTION_TIME_OUT = 20;
    int SOCKET_TIME_OUT = 25;

    //TODO Test
    //String BASE_URL = "http://192.168.1.72:8400/";

    //TODO Live
    String BASE_URL = "http://34.253.215.164:8956/";

    String URL_LOGIN = BASE_URL + "login";
    String URL_SIGN_UP = BASE_URL + "members";

    String URL_REFRESH_TOKEN = BASE_URL + "login";
    String URL_GET_ALL_CITIZEN = BASE_URL + "citizens";
    String URL_POST_ADD_CITIZEN = BASE_URL + "citizens";
    String URL_POST_ADD_INCIDENT = BASE_URL + "incidents";
    String URL_POST_RESET_PASSWORD = BASE_URL + "reset-password";
    String URL_GET_ALL_MY_CITIZEN = BASE_URL + "incidents";
    String URL_GET_ALL_INCIDENT_CATEGORY = BASE_URL + "categories";

    String URL_GET_REGIONS = BASE_URL + "regions";
    String URL_GET_ADVOCATION = BASE_URL + "advocations";

    String URL_GET_CONSTITUENCIES = BASE_URL + "constituencies";
    String URL_GET_ELECTORAL = BASE_URL + "electoral-area";
    String URL_GET_BRANCH_NAME = BASE_URL + "branch-names";
    String URL_PUT_VOTED_OR_STAND_BY_CITIZEN = BASE_URL + "citizens";

    int IMAGE_TYPE_FIRST = 1;
    int IMAGE_TYPE_SECOND = 2;

    int STAND_BY = 1;
    int VOTED = 2;


    //NEW API

    String URL_GET_ALL_CITIZEN_NEW = BASE_URL + "voters";

    String URL_GET_ALL_CITIZEN_NEW_SEARCH = BASE_URL + "voters";


}
