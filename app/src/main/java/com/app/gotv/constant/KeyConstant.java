package com.app.gotv.constant;

public interface KeyConstant {

    String KEY_RESPONSE = "response";
    String KEY_RESULT = "result";
    String KEY_MESSAGE = "message";
    String KEY_ERROR_TYPE = "error_type";
    String KEY_STATUS = "status";
    String KEY_SUCCESS = "success";
    String KEY_ERROR = "error";
    String KEY_RESPONSE_CODE_1 = "1";
    String KEY_RESPONSE_CODE_0 = "0";
    String KEY_DATA = "data";
    String KEY_FLAG = "flag";


    String KEY_RESPONSE_CODE_200 = "200";
    String KEY_RESPONSE_CODE_202 = "202";
    String KEY_RESPONSE_CODE_203 = "203";
    String KEY_RESPONSE_CODE_205 = "205";

    String KEY_RESPONSE_CODE_201 = "201";
    String KEY_RESPONSE_CODE_400 = "400";
    String KEY_RESPONSE_CODE_401 = "401";
    String KEY_QUESTION = "question";
    String HEADER_AUTHORIZATION = "Authorization";


    String KEY_MESSAGE_FALSE = "false";
    String KEY_TOKEN_TYPE = "token_type";
    String KEY_LOGIN_STATUS = "login_status";
    String KEY_ACCESS_TOKEN = "accessToken";
    String KEY_REFRESH_TOKEN = "refreshToken";

    String KEY_GRANT_TYPE = "grant_type";
    String KEY_USER_NAME = "username";
    String KEY_PASSWORD = "password";
    String KEY_SCOPE = "scope";
    String KEY_USER = "user";

    String KEY_NAME = "name";
    String KEY_VOTER_ID = "voter_id";
    String KEY_DOB = "DOB";
    String KEY_GENDER = "gender";
    String KEY_OCCUPATION = "occupation";
    String KEY_CONTACT = "contact";
    String KEY_ADDRESS = "address";
    String KEY_EMAIL = "email";
    String KEY_LOCATION = "location";

    String KEY_REFERRED_BY = "referred_by";
    String KEY_REFERRED_TO = "referred_to";
    String KEY_REGION = "region";
    String KEY_CONSTITUENCY = "constituency";
    String KEY_ELECTORAL_AREA = "electoral_area";
    String KEY_ELECTORAL = "electoral";
    String KEY_ADVOCATING = "advocation";
    String KEY_POSITION_DETAIL = "position_status";

    String KEY_REGION_ID = "region_id";
    String KEY_CONSTITUENCY_ID = "constituency_id";
    String KEY_ELECTORAL_ID = "electoral_area_id";
    String KEY_BRANCH_ID = "branch_name_id";
    String KEY_DESCRIPTION = "description";
    String KEY_HEADLINE = "headline";
    String KEY_UPDATE_DATE = "updatedAt";

    String KEY_CATEGORY = "categories_id";

    String KEY_BRANCH_NAME = "branchName";
    String KEY_BRANCH = "branch";

    String KEY_BRANCH_CODE = "branch_code";


    String KEY_IMAGE = "image";
    String KEY_VIDEO = "video";
    String KEY_IS_APP = "isApp";
    String KEY_ID = "_id";
    String KEY_MEMBER_ID = "member_id";
    String KEY_FIRST_LOGIN = "first_login";
    String KEY_SUB_CATEGORY_ID = "sub_category_id";


    String KEY_POLLING_STATION = "polling_station";

    String KEY_SEX = "sex";
    String KEY_AGE = "age";

    String KEY_DOCS = "docs";


}
