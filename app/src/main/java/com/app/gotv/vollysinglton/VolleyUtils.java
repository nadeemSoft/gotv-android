package com.app.gotv.vollysinglton;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.gotv.R;
import com.app.gotv.activity.HomeActivity;
import com.app.gotv.activity.LoginActivity;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.sessionManagement.AppSession;
import com.google.gson.JsonObject;


import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by codezilla-8 on 20/4/18.
 */

public class VolleyUtils {
    AppSession appSession;
    String accessToken;

    public void GET_METHOD(final Context context, final String url, final VolleyResponseListener listener) {
        appSession = AppSession.getInstance(context);
        accessToken = appSession.getAccessToken();

        Log.e("AccessToken", accessToken);
        Log.e("url", url);

        StringRequest stringRequest = null;
        final StringRequest finalStringRequest = stringRequest;
        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject obj = new JSONObject(response);
                    int status = obj.optInt(KeyConstant.KEY_STATUS);
                    if (status == 401) {
                        if (appSession.getRefreshToken() != null) {
                            RefreshTokenGetMethod(context, url, finalStringRequest, listener);
                        } else {
                            Intent in = new Intent(context, LoginActivity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(in);
                        }


                        return;
                    } else {
                        listener.onResponse(response);
                        return;
                    }

                } catch (Exception e) {
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            try {

                                JSONObject objecterror = new JSONObject(jsonError);
                                String message = objecterror.optString("message");

                                int status = objecterror.optInt(KeyConstant.KEY_STATUS);

                                if (status == 401) {
                                    if (appSession.getRefreshToken() != null) {
                                        RefreshTokenGetMethod(context, url, finalStringRequest, listener);
                                    } else {
                                        Intent in = new Intent(context, LoginActivity.class);
                                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        context.startActivity(in);
                                    }


                                    return;
                                } else {
                                    listener.onError(message);

                                }


                            } catch (Exception e) {

                            }


                            listener.onError(jsonError);

                        }

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + accessToken);

                Log.e("headers", headers + "");

                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through singleton class.
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    public void GET_METHOD_WITH_REGION_CONSTITUENCY_BRANCH_NAME(final Context context, final String url, final VolleyResponseListener listener) {
        appSession = AppSession.getInstance(context);
        accessToken = appSession.getAccessToken();

        //Log.e("AccessToken", accessToken);
        Log.e("url", url);

        StringRequest stringRequest = null;
        final StringRequest finalStringRequest = stringRequest;
        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject obj = new JSONObject(response);
                    int status = obj.optInt(KeyConstant.KEY_STATUS);
                    if (status == 401) {

                       /* if (appSession.getRefreshToken() != null) {
                            RefreshTokenGetMethod(context, url, finalStringRequest, listener);
                        } else {
                            Intent in = new Intent(context, LoginActivity.class);
                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(in);
                        }
*/

                        return;
                    } else {
                        listener.onResponse(response);
                        return;
                    }

                } catch (Exception e) {
                    Log.e("Exception",e+"");
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.data != null) {
                            String jsonError = new String(networkResponse.data);
                            try {

                                JSONObject objecterror = new JSONObject(jsonError);
                                String message = objecterror.optString("message");

                                int status = objecterror.optInt(KeyConstant.KEY_STATUS);

                                if (status == 401) {
                                   /* if (appSession.getRefreshToken() != null) {
                                        RefreshTokenGetMethod(context, url, finalStringRequest, listener);
                                    } else {
                                        Intent in = new Intent(context, LoginActivity.class);
                                        in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        context.startActivity(in);
                                    }*/


                                    return;
                                } else {
                                    listener.onError(message);

                                }


                            } catch (Exception e) {

                            }


                            listener.onError(jsonError);

                        }

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + accessToken);

                Log.e("headers", headers + "");

                return headers;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Access the RequestQueue through singleton class.
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    public void POST_METHOD(final Context context, final boolean isLogin, final String url, final Map<String, String> getParams, final VolleyResponseListener listener) {
        appSession = AppSession.getInstance(context);
        accessToken = appSession.getAccessToken();
        StringRequest stringRequest = null;
        final StringRequest finalStringRequest = stringRequest;
        stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            int status = obj.optInt(KeyConstant.KEY_STATUS);
                            if (status == 401) {
                                if (appSession.getRefreshToken() != null) {
                                    RefreshToken(context, isLogin, url, getParams, finalStringRequest, listener);

                                } else {
                                    Intent in = new Intent(context, LoginActivity.class);
                                    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    context.startActivity(in);
                                }
                                return;
                            } else {
                                listener.onResponse(response);
                                return;
                            }

                        } catch (Exception e) {
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        try {
                            NetworkResponse networkResponse = error.networkResponse;

                            if (networkResponse != null && networkResponse.data != null) {
                                String jsonError = new String(networkResponse.data);
                                Log.e("Error", jsonError);
                                try {
                                    JSONObject obj = new JSONObject(jsonError);
                                    int status = obj.optInt(KeyConstant.KEY_STATUS);
                                    String message = obj.optString(KeyConstant.KEY_MESSAGE);
                                    if (status == 401) {
                                        if (appSession.getRefreshToken() != null) {
                                            RefreshToken(context, isLogin, url, getParams, finalStringRequest, listener);

                                        } else {
                                            Intent in = new Intent(context, LoginActivity.class);
                                            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            context.startActivity(in);
                                        }
                                        return;
                                    } else {
                                        listener.onError(message);

                                    }
                                } catch (Exception e) {
                                    Log.e("Exception", e + "");
                                }
                            }
                        } catch (Exception e) {
                            Log.e("Exception", e + "");

                        }

                    }
                }) {
            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                if (isLogin) {
                    headers.put("Authorization", "Basic REVNTzpERU1PMTIz");
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                } else {
                    headers.put("Authorization", "Bearer " + accessToken);
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                }

                Log.e("Header", headers + "");
                return headers;
            }

            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                return getParams;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Access the RequestQueue through singleton class.
        MySingleton.getInstance(context).addToRequestQueue(stringRequest);
    }


    public void POST_METHOD_WITH_JSON(final Context context, final String url, final JSONObject jsonBody, final VolleyResponseListener listener) {


        appSession = AppSession.getInstance(context);
        accessToken = appSession.getAccessToken();


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                try {

                    try {
                        JSONObject obj = new JSONObject(String.valueOf(response));
                        int status = obj.optInt(KeyConstant.KEY_STATUS);
                        if (status == 401) {
                            RefreshTokenJsonObject(context, url, jsonBody, listener);
                            return;
                        } else {
                            listener.onResponse(response);
                            return;
                        }

                    } catch (Exception e) {
                    }


                } catch (Exception e) {
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");

                try {
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);
                        Log.e("Error", jsonError);
                        try {
                            JSONObject obj = new JSONObject(jsonError);
                            String message = obj.optString("message");
                            if (obj.optString("error_code").equalsIgnoreCase("401")) {
                                // RefreshTokenGetMethod(context, url, finalStringRequest, listener);
                                return;
                            } else {
                                listener.onError(message);

                            }
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e) {
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + accessToken);
                headers.put("Content-Type", "application/json");
                Log.e("headers", headers + "");

                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjectRequest);


    }


    public void PUT_METHOD_WITH_JSON(final Context context, final String url, JSONObject jsonBody, final VolleyResponseListener listener) {


        appSession = AppSession.getInstance(context);
        accessToken = appSession.getAccessToken();
        Log.e("url", url);


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                try {

                    if (response.optString("error_code").equalsIgnoreCase("401")) {
                        // RefreshToken(context, url, getParams, finalStringRequest, listener);
                        return;
                    } else {
                        listener.onResponse(response);
                        return;
                    }

                } catch (Exception e) {
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");

                try {
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);
                        Log.e("Error", jsonError);
                        try {
                            JSONObject obj = new JSONObject(jsonError);
                            String message = obj.optString("message");
                            if (obj.optString("error_code").equalsIgnoreCase("401")) {
                                // RefreshTokenGetMethod(context, url, finalStringRequest, listener);
                                return;
                            } else {
                                listener.onError(message);

                            }
                        } catch (Exception e) {
                        }
                    } else {
                        listener.onError(networkResponse.toString());

                    }
                } catch (Exception e) {
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Bearer " + accessToken);
                headers.put("Content-Type", "application/json");
                Log.e("headers", headers + "");

                return headers;
            }


        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjectRequest);


    }


    public void RefreshToken(final Context context, final boolean isLogin, final String url, final Map<String, String> getParams, final StringRequest stringRequest, final VolleyResponseListener listener) {

        RequestQueue queue = Volley.newRequestQueue(context);

        Log.e("url_REFRESH_", UrlConstant.URL_REFRESH_TOKEN);

        StringRequest sr = new StringRequest(Request.Method.POST, UrlConstant.URL_REFRESH_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.e("RefreshResponse", response + "");
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String accessToken = jsonObject.optString(KeyConstant.KEY_ACCESS_TOKEN);
                        String refreshToken = jsonObject.optString(KeyConstant.KEY_REFRESH_TOKEN);
                        appSession.setToken(accessToken, refreshToken);
                        appSession.setUserLogin(true);
                        JSONObject objectUser = jsonObject.optJSONObject(KeyConstant.KEY_USER);
                        String name = objectUser.optString(KeyConstant.KEY_NAME);
                        String image = objectUser.optString(KeyConstant.KEY_IMAGE);
                        appSession.setMyName(name);
                        appSession.setUserImage(image);

                        POST_METHOD(context, isLogin, url, getParams, new VolleyResponseListener() {
                            @Override
                            public void onError(String message) {
                                listener.onError(message);
                                Intent in = new Intent(context, LoginActivity.class);
                                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(in);
                            }

                            @Override
                            public void onResponse(Object response) {
                                listener.onResponse(response);
                                Log.e("respose", response.toString());
                            }
                        });


                    } catch (Exception e) {

                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();


                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("grant_type", "refresh_token");
                params.put("scope", "2");
                params.put("userType", "1");
                params.put("refresh_token", appSession.getRefreshToken());

                Log.e("params", params.toString());
                return params;
            }
        };

        sr.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                0));
        queue.add(sr);
    }

    public void RefreshTokenGetMethod(final Context context, final String url, final StringRequest stringRequest, final VolleyResponseListener listener) {

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest sr = new StringRequest(Request.Method.POST, UrlConstant.URL_REFRESH_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    Log.e("resposeREFRESH", response.toString());

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String accessToken = jsonObject.optString(KeyConstant.KEY_ACCESS_TOKEN);
                        String refreshToken = jsonObject.optString(KeyConstant.KEY_REFRESH_TOKEN);
                        appSession.setToken(accessToken, refreshToken);
                        appSession.setUserLogin(true);
                        JSONObject objectUser = jsonObject.optJSONObject(KeyConstant.KEY_USER);
                        String name = objectUser.optString(KeyConstant.KEY_NAME);
                        String image = objectUser.optString(KeyConstant.KEY_IMAGE);
                        appSession.setMyName(name);
                        appSession.setUserImage(image);
                        GET_METHOD(context, url, new VolleyResponseListener() {
                            @Override
                            public void onError(String message) {
                                listener.onError(message);
                                Intent in = new Intent(context, LoginActivity.class);
                                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(in);

                            }

                            @Override
                            public void onResponse(Object response) {
                                listener.onResponse(response);
                                Log.e("respose", response.toString());
                            }
                        });

                    } catch (Exception e) {
                    }

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", String.valueOf(error));
                listener.onResponse(String.valueOf(error));

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "Basic REVNTzpERU1PMTIz");
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("HEADERrEFERSH", headers + "");

                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("grant_type", "refresh_token");
                params.put("scope", "2");
                params.put("userType", "1");
                params.put("refresh_token", appSession.getRefreshToken());

                Log.e("params", params.toString());
                return params;
            }
        };

        sr.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                0));
        queue.add(sr);
    }


    public void RefreshTokenJsonObject(final Context context, final String url, final JSONObject jsonObjectMain, final VolleyResponseListener listener) {

        RequestQueue queue = Volley.newRequestQueue(context);

        Log.e("url_REFRESH_", UrlConstant.URL_REFRESH_TOKEN);

        StringRequest sr = new StringRequest(Request.Method.POST, UrlConstant.URL_REFRESH_TOKEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {

                    Log.e("RefreshResponse", response + "");
                    try {

                        JSONObject jsonObject = new JSONObject(response);
                        String accessToken = jsonObject.optString(KeyConstant.KEY_ACCESS_TOKEN);
                        String refreshToken = jsonObject.optString(KeyConstant.KEY_REFRESH_TOKEN);
                        appSession.setToken(accessToken, refreshToken);
                        appSession.setUserLogin(true);
                        JSONObject objectUser = jsonObject.optJSONObject(KeyConstant.KEY_USER);
                        String name = objectUser.optString(KeyConstant.KEY_NAME);
                        String image = objectUser.optString(KeyConstant.KEY_IMAGE);
                        appSession.setMyName(name);
                        appSession.setUserImage(image);

                        POST_METHOD_WITH_JSON(context, url, jsonObjectMain, new VolleyResponseListener() {
                            @Override
                            public void onError(String message) {
                                listener.onError(message);
                                Intent in = new Intent(context, LoginActivity.class);
                                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                context.startActivity(in);
                            }

                            @Override
                            public void onResponse(Object response) {
                                listener.onResponse(response);
                                Log.e("respose", response.toString());
                            }
                        });

                    } catch (Exception e) {

                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("error", String.valueOf(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();


                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                params.put("grant_type", "refresh_token");
                params.put("scope", "2");
                params.put("userType", "1");
                params.put("refresh_token", appSession.getRefreshToken());

                Log.e("params", params.toString());
                return params;
            }
        };

        sr.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                0));
        queue.add(sr);
    }

    public void POST_METHOD_WITH_JSON_SIGN_UP(final Context context, final String url, final JSONObject jsonBody, final VolleyResponseListener listener) {


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                try {

                    try {
                        JSONObject obj = new JSONObject(String.valueOf(response));
                        int status = obj.optInt(KeyConstant.KEY_STATUS);
                        if (status == 401) {
                            //RefreshTokenJsonObject(context, url, jsonBody, listener);
                            Toast.makeText(context, ""+context.getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            listener.onResponse(response);
                            return;
                        }

                    } catch (Exception e) {
                    }


                } catch (Exception e) {
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");

                try {
                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);
                        Log.e("Error", jsonError);
                        try {
                            JSONObject obj = new JSONObject(jsonError);
                            String message = obj.optString("message");
                            if (obj.optString("error_code").equalsIgnoreCase("401")) {
                                // RefreshTokenGetMethod(context, url, finalStringRequest, listener);
                                return;
                            } else {
                                listener.onError(message);

                            }
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e) {
                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                Log.e("headers", headers + "");

                return headers;
            }
        };


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonObjectRequest);


    }

}
