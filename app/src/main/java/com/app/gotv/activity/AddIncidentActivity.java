package com.app.gotv.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.app.gotv.R;
import com.app.gotv.adapter.AdapterRegionsConstituencyBranch;
import com.app.gotv.adapter.MyIncidentAdapter;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.runtimepermission.PermissionsManager;
import com.app.gotv.runtimepermission.PermissionsResultAction;
import com.app.gotv.sessionManagement.AppSession;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.vollysinglton.VolleyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO;

public class AddIncidentActivity extends AppCompatActivity implements View.OnClickListener {
    private Activity activity = AddIncidentActivity.this;
    final private static int REQUEST_CODE_SELECT_IMAGE = 10;
    final private static int REQUEST_CODE_SELECT_VIDEO = 11;
    final private static int REQUEST_CODE_TAKE_IMAGE = 12;
    final private static int REQUEST_CODE_TAKE_VIDEO = 13;


    private EditText etDescription = null, etHeadline = null, etRegion = null, etConstituency = null, etElectoral = null, etBranchName = null;
    private TextView tvToolBar = null, tvSubCategory = null, tvCategory = null;
    private ImageView ivBack = null, ivIncidentImage = null, ivPlayVideo = null;
    private FrameLayout flSelectImVideo = null;
    public String[] mPermission = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private PermissionsManager permissionsManager;
    private VolleyUtils volleyUtils = null;
    private AppSession appSession = null;
    private Button btnAddIncident = null;
    private String idRegions = "", idConstituency = "", idElectoral = "", idBranchName = "", branchCode = "";

    private String idCategory = "", idSubcategory = "";
    private String mediaFilePath;
    private int selectedPosition;
    private Uri fileUri;
    private Bitmap imageBitmap;
    private int selectedItemPosition = -1;
    private int MAX_HEIGHT = 256;
    private int MAX_WIDTH = 256;
    private int mediaType = 0;
    private Dialog progressDialog = null, dialogRegionConstituencyBranchName = null;
    private boolean isCategory = false, isSubCategory = false;
    private LinearLayout llSubCategory = null;
    private JSONArray jsonArraySub = null;
    private int IMAGE = 1;
    private int VIDEO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_incident);
        initWidgets();
        permissionsManager = PermissionsManager.getInstance();
        AWSMobileClient.getInstance().initialize(this).execute();
        volleyUtils = new VolleyUtils();
        appSession = AppSession.getInstance(activity);
        updateUI();
    }

    private void initWidgets() {
        ivBack = findViewById(R.id.iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);
        tvToolBar = findViewById(R.id.tv_toolbar);
        tvToolBar.setText("" + activity.getResources().getString(R.string.add_incident));
        etHeadline = findViewById(R.id.et_incident_headline);
        etDescription = findViewById(R.id.et_incident_description);
        tvCategory = findViewById(R.id.et_incident_category);
        tvSubCategory = findViewById(R.id.et_incident_sub_category);
        tvSubCategory.setOnClickListener(this);
        etRegion = findViewById(R.id.et_add_region);
        etConstituency = findViewById(R.id.et_add_constituency);
        etElectoral = findViewById(R.id.et_add_electoral);
        etBranchName = findViewById(R.id.et_add_brance_name);
        ivIncidentImage = findViewById(R.id.act_repo_inc_im);
        ivPlayVideo = findViewById(R.id.act_repo_inc_im_play);
        flSelectImVideo = findViewById(R.id.act_repo_inc_fl);
        btnAddIncident = findViewById(R.id.btn_add_incident);
        llSubCategory = findViewById(R.id.ll_incident_subCategory);
        btnAddIncident.setOnClickListener(this);
        flSelectImVideo.setOnClickListener(this);
        tvCategory.setOnClickListener(this);
        progressDialog = AppUtil.showLoaderDialog(activity, false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.et_incident_category:
                isCategory = true;
                isSubCategory = false;
                getCategory();
                break;

            case R.id.et_incident_sub_category:
                if (jsonArraySub != null) {
                    isCategory = false;
                    isSubCategory = true;
                    showDataRegionsConstituenctBranchName(jsonArraySub);

                }
                break;

            case R.id.btn_add_incident:
                if (checkValidation()) {
                    progressDialog.show();

                    reportIncidentAPI();
                }
                break;

            case R.id.act_repo_inc_fl:
                String[] strings = {"Select Image", "Select Video", "Take Image", "Record Video"};
                new AlertDialog.Builder(activity)
                        .setSingleChoiceItems(strings, 0, null)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();

                                selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();

                                if (permissionsManager.hasAllPermissions(activity, mPermission)) {

                                    mediaIntent();
                                } else {

                                    permissionsManager.requestPermissionsIfNecessaryForResult(activity, mPermission, new PermissionsResultAction() {
                                        @Override
                                        public void onGranted() {
                                            mediaIntent();
                                        }

                                        @Override
                                        public void onDenied(String permission) {

                                        }
                                    });
                                }

                            }
                        }).
                        setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();


                            }
                        })
                        .show();
                break;

        }

    }


    private void mediaIntent() {

        if (selectedPosition == 0) {

            Intent intent2 = new Intent();
            intent2.setType("image/*");
            intent2.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent2, "Select Picture From Gallery"), REQUEST_CODE_SELECT_IMAGE);

        } else if (selectedPosition == 1) {

            Intent intent2 = new Intent();
            intent2.setType("video/*");
            intent2.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent2, "Select Video From Gallery"), REQUEST_CODE_SELECT_VIDEO);
        } else if (selectedPosition == 2) {

            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(cameraIntent, REQUEST_CODE_TAKE_IMAGE);

        } else if (selectedPosition == 3) {

            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(intent, REQUEST_CODE_TAKE_VIDEO);
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            Uri selectedImage;
            Cursor cursor;
            String[] filePathColumn = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
            int columnIndex;

            switch (requestCode) {

                case REQUEST_CODE_SELECT_IMAGE:

                    selectedImage = data.getData();
                    cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    mediaFilePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.e("PATH", "" + mediaFilePath);

                    try {
                        imageBitmap = handleSamplingAndRotationBitmap(activity, selectedImage);
                        ivIncidentImage.setImageBitmap(imageBitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ivIncidentImage.setVisibility(View.VISIBLE);
                    ivPlayVideo.setVisibility(View.GONE);
                    mediaType = IMAGE;

                    break;

                case REQUEST_CODE_TAKE_IMAGE:

                    Uri uri = FileProvider.getUriForFile(activity, getApplicationContext().getPackageName() + ".provider", new File(mediaFilePath));
                    try {

                        imageBitmap = handleSamplingAndRotationBitmap(activity, uri);
                        ivIncidentImage.setImageBitmap(imageBitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ivIncidentImage.setVisibility(View.VISIBLE);
                    ivPlayVideo.setVisibility(View.GONE);
                    mediaType = IMAGE;

                    break;

                case REQUEST_CODE_SELECT_VIDEO:

                    selectedImage = data.getData();
                    filePathColumn[0] = MediaStore.Video.Media.DATA;

                    cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    columnIndex = cursor.getColumnIndex(filePathColumn[1]);
                    mediaFilePath = cursor.getString(columnIndex);
                    cursor.close();
                    Log.e("PATH", "" + mediaFilePath);

                    imageBitmap = ThumbnailUtils.createVideoThumbnail(mediaFilePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);

                    mediaType = VIDEO;

                    ivIncidentImage.setImageBitmap(imageBitmap);
                    ivIncidentImage.setVisibility(View.VISIBLE);
                    ivPlayVideo.setVisibility(View.VISIBLE);


                    break;

                case REQUEST_CODE_TAKE_VIDEO:

                    imageBitmap = ThumbnailUtils.createVideoThumbnail(mediaFilePath, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
                    ivIncidentImage.setImageBitmap(imageBitmap);
                    mediaType = VIDEO;

                    ivIncidentImage.setVisibility(View.VISIBLE);
                    ivPlayVideo.setVisibility(View.VISIBLE);

                    break;

            }
        }
    }

    private void updateUI() {

        String regionKey = appSession.getRegionData();
        String electorailKey = appSession.getElectorailData();
        String constituencyKey = appSession.getConstituencyData();
        String branchKey = appSession.getBranchData();


        try {
            JSONObject objectRegion = new JSONObject(regionKey);
            idRegions = objectRegion.optString(KeyConstant.KEY_ID);
            String nameRegion = objectRegion.optString(KeyConstant.KEY_NAME);
            etRegion.setText(nameRegion);

            JSONObject objectElectorail = new JSONObject(electorailKey);

            idElectoral = objectElectorail.optString(KeyConstant.KEY_ID);
            String nameElectorial = objectElectorail.optString(KeyConstant.KEY_NAME);
            etElectoral.setText(nameElectorial);

            JSONObject objectConstituency = new JSONObject(constituencyKey);

            idConstituency = objectConstituency.optString(KeyConstant.KEY_ID);
            String nameConstituency = objectConstituency.optString(KeyConstant.KEY_NAME);
            etConstituency.setText(nameConstituency);

            JSONObject objectBranch = new JSONObject(branchKey);

            idBranchName = objectBranch.optString(KeyConstant.KEY_ID);
            String nameBranch = objectBranch.optString(KeyConstant.KEY_NAME);

            etBranchName.setText(nameBranch);


        } catch (Exception e) {
            Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();

        }

    }

    private void reportIncidentAPI() {

        if (AppUtil.isNetworkAvailable(activity)) {

            AppUtil.hideSoftKeyboard(activity);
            String imagePath = AppUtil.createImageFromBitmap(imageBitmap);
            uploadWithTransferUtilityForImage(new File(imagePath));

        } else {
            progressDialog.dismiss();

            Toast.makeText(activity, "" + getResources().getString(R.string.please_check_internet), Toast.LENGTH_LONG).show();
        }
    }

    private void uploadWithTransferUtilityForImage(File file) {

        final String imageKey = "gotv-test" + "/" + file.getName();
        Log.e("imageKey", imageKey);

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                        .build();

        TransferObserver uploadObserver = transferUtility.upload(imageKey, file);

        uploadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {

                    if (mediaType == IMAGE) {

                        createRequest(imageKey, "");

                    } else if (mediaType == VIDEO) {

                        File file = new File(mediaFilePath);
                        uploadWithTransferUtilityForVideo(file, imageKey);
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;

                Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent
                        + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                Toast.makeText(activity, "Error in uploading media", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

        });

    }

    private void uploadWithTransferUtilityForVideo(File file, final String imageKey) {

        final String videoKey = "gotv-test" + "/" + file.getName();
        Log.e("videoKey", videoKey);

        TransferUtility transferUtility =
                TransferUtility.builder()
                        .context(getApplicationContext())
                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))
                        .build();

        TransferObserver uploadObserver = transferUtility.upload(videoKey, file);

        uploadObserver.setTransferListener(new TransferListener() {

            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {

                    createRequest(imageKey, videoKey);
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                int percentDone = (int) percentDonef;

                Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent
                        + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
            }

            @Override
            public void onError(int id, Exception ex) {
                Toast.makeText(activity, "Error in uploading media", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();

            }

        });

    }

    private void createRequest(String imageKey, String videoKey) {


        try {
            JSONObject object = new JSONObject();

            object.put(KeyConstant.KEY_DESCRIPTION, etDescription.getText().toString());
            object.put(KeyConstant.KEY_HEADLINE, etHeadline.getText().toString());

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(idSubcategory);

            object.put(KeyConstant.KEY_CATEGORY, jsonArray);


            object.put(KeyConstant.KEY_REGION, idRegions);
            object.put(KeyConstant.KEY_ELECTORAL_AREA, idElectoral);
            object.put(KeyConstant.KEY_CONSTITUENCY, idConstituency);
            object.put(KeyConstant.KEY_BRANCH, idBranchName);

            if (videoKey != "") {
                JSONObject objectMedia = new JSONObject();
                objectMedia.put("image", imageKey);
                objectMedia.put("video", videoKey);

                object.put("media", objectMedia);

            } else {
                JSONObject objectMedia = new JSONObject();
                objectMedia.put("image", imageKey);

                object.put("media", objectMedia);

            }


            Log.e("Object", object.toString());


            volleyUtils.POST_METHOD_WITH_JSON(activity, UrlConstant.URL_POST_ADD_INCIDENT, object, new VolleyResponseListener() {
                @Override
                public void onError(String message) {
                    progressDialog.dismiss();
                    AppUtil.showCommonPopup(activity, message);

                }

                @Override
                public void onResponse(Object response) {
                    Log.e("AddCitizenResponse", response + "");
                    progressDialog.dismiss();

                    try {
                        JSONObject objectResponse = new JSONObject(String.valueOf(response));
                        int status = objectResponse.optInt(KeyConstant.KEY_STATUS);
                        String message = objectResponse.optString(KeyConstant.KEY_MESSAGE);
                        if (status == 200) {
                            //showCommonPopup(activity,message);
                            Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();
                            finish();


                        } else {
                            // showCommonPopup(activity,message);
                            Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();

                            finish();


                        }

                    } catch (Exception e) {

                    }


                }
            });


        } catch (Exception e) {
            Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
        }


    }

    private void getCategory() {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_INCIDENT_CATEGORY;
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                Log.e("IncidentCategory", response + "");

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {
                        progressDialog.dismiss();

                        JSONArray jsonArray = jsonObject.optJSONArray(KeyConstant.KEY_RESPONSE);

                        showDataRegionsConstituenctBranchName(jsonArray);


                    } else {
                        progressDialog.dismiss();

                        AppUtil.showCommonPopup(activity, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();

                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });
    }


    private void showDataRegionsConstituenctBranchName(JSONArray jsonArray) {
        dialogRegionConstituencyBranchName = new Dialog(activity);
        dialogRegionConstituencyBranchName.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRegionConstituencyBranchName.setCancelable(true);
        dialogRegionConstituencyBranchName.setContentView(R.layout.dialog_regions_constituency_branch);
        RecyclerView recyclerView = dialogRegionConstituencyBranchName.findViewById(R.id.rv_regions);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));

        AdapterRegionsConstituencyBranch adapterCategoryCreatAds = new AdapterRegionsConstituencyBranch(activity, jsonArray);
        recyclerView.setAdapter(adapterCategoryCreatAds);

        dialogRegionConstituencyBranchName.show();

    }


    public void setIdRegionsConstituencyBranch(int position, JSONArray jsonArray) {
        dialogRegionConstituencyBranchName.dismiss();

        JSONObject objectChild = jsonArray.optJSONObject(position);
        jsonArraySub = objectChild.optJSONArray(KeyConstant.KEY_SUB_CATEGORY_ID);



        if (isCategory) {


            if (jsonArraySub != null) {
                if (jsonArraySub.length() > 0) {
                    llSubCategory.setVisibility(View.VISIBLE);
                } else {
                    llSubCategory.setVisibility(View.GONE);

                }

            } else {
                llSubCategory.setVisibility(View.GONE);

            }

            idCategory = objectChild.optString(KeyConstant.KEY_ID);
            String name = objectChild.optString(KeyConstant.KEY_NAME);
            tvCategory.setText("" + name);

            Log.e("idCategory", idCategory + "");
            Log.e("CategoryName", name + "");


        } else if (isSubCategory) {

            idSubcategory = objectChild.optString(KeyConstant.KEY_ID);
            String name = objectChild.optString(KeyConstant.KEY_NAME);
            tvSubCategory.setText("" + name);

            Log.e("idSubcategory", idSubcategory + "");
            Log.e("SubCategoryName", name + "");


        }
    }


    private boolean checkValidation() {
        if (etHeadline.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_headline));
            return false;
        } else if (etDescription.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_description));
            return false;
        } else if (idCategory.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_category));
            return false;
        }else if (idSubcategory.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_subcategory));
            return false;
        } else if (idRegions.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_region));
            return false;
        } else if (idConstituency.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_constituency));
            return false;
        } else if (idElectoral.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_electoral));
            return false;
        } else if (idBranchName.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_branch_name));
            return false;
        } else if (imageBitmap == null) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_select_image_video));
            return false;
        }
        return true;

    }


    public Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage) throws IOException {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);

        return img;
    }


    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
     /*   final int height = options.outHeight;
        final int width = options.outWidth;*/
        final int height = 512;
        final int width = 512;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(mediaFilePath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    public Uri getOutputMediaFileUri(int type) {

        mediaFilePath = getOutputMediaFile(type).getPath();
        Log.e("fileUriPath", mediaFilePath);
        Uri uri = FileProvider.getUriForFile(activity, getApplicationContext().getPackageName() + ".provider", new File(mediaFilePath));
        return uri;

    }


    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(android.os.Environment.getExternalStorageDirectory(), "/Gotv");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Hello Camera", "Oops! Failed create Hello Camera directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile = null;
        if (type == MEDIA_TYPE_IMAGE) {

            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {

            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        }

        return mediaFile;
    }
}
