package com.app.gotv.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.app.gotv.R;
import com.app.gotv.adapter.MyCitizenAdapter;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.fragment.HomeFragment;
import com.app.gotv.fragment.MyCitizenFragment;
import com.app.gotv.fragment.MyIncidentFragment;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.runtimepermission.PermissionsManager;
import com.app.gotv.runtimepermission.PermissionsResultAction;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.utils.GPSTracker;
import com.app.gotv.vollysinglton.VolleyUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    public static int goldValue = 0;
    private Activity activity = HomeActivity.this;
    private TextView tvBatch = null, tvNoCitizen = null;
    private LinearLayout llHome = null, llMyCitizen = null, llMyIncident = null;
    private Fragment homeFragment = null, myCitizenFragment = null, myIncidentFragment = null;
    private FloatingActionButton actionButton = null;
    private ImageView ivHome = null, ivMyCitizen = null, ivMyIncident;
    private Dialog progressDialog = null;
    private JSONArray jsonArrayResponse = null;
    public String[] mPermission = {Manifest.permission.ACCESS_FINE_LOCATION};
    public PermissionsManager permissionsManager;
    boolean isIncident =false,isMyVerifiend = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initWidgets();
        permissionsManager = PermissionsManager.getInstance();
        permission();

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.fab:

                if (isMyVerifiend){
                    if (jsonArrayResponse != null) {
                        if (goldValue >= 5) {
                            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.not_Add_citizen));

                        } else {
                            Intent intent = new Intent(activity, AddCitizenActivity.class);
                            activity.startActivity(intent);

                        }


                    } else {
                        Intent intent = new Intent(activity, AddCitizenActivity.class);
                        activity.startActivity(intent);
                    }
                }else if (isIncident){
                    Intent intent = new Intent(activity, AddIncidentActivity.class);
                    activity.startActivity(intent);
                }



                break;

            case R.id.act_home_ll_home:

                llHome.setBackground(getResources().getDrawable(R.drawable.round_corner_status));
                llMyCitizen.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
                llMyIncident.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
                ivHome.setColorFilter(getResources().getColor(R.color.colorMainBackground));
                actionButton.setVisibility(View.VISIBLE);
                isIncident = false;
                isMyVerifiend = true;
                homeFragment = new HomeFragment();
                loadFragment(homeFragment);

                break;

            case R.id.act_home_ll_my_citizen:
                llHome.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
                llMyCitizen.setBackground(getResources().getDrawable(R.drawable.round_corner_status));
                llMyIncident.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
                isIncident = false;
                isMyVerifiend = true;
                actionButton.setVisibility(View.VISIBLE);

                myCitizenFragment = new MyCitizenFragment();
                loadFragment(myCitizenFragment);

                break;

            case R.id.act_home_ll_my_incident:

                llHome.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
                llMyCitizen.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
                llMyIncident.setBackground(getResources().getDrawable(R.drawable.round_corner_status));

                actionButton.setVisibility(View.VISIBLE);
                isIncident = true;
                isMyVerifiend = false;
                myIncidentFragment = new MyIncidentFragment();
                loadFragment(myIncidentFragment);
                break;
        }

    }

    @SuppressLint("RestrictedApi")
    private void initWidgets() {

        llHome = findViewById(R.id.act_home_ll_home);
        llMyCitizen = findViewById(R.id.act_home_ll_my_citizen);
        llMyIncident = findViewById(R.id.act_home_ll_my_incident);

        actionButton = findViewById(R.id.fab);
        actionButton.setVisibility(View.VISIBLE);
        actionButton.setOnClickListener(this);

        ivHome = findViewById(R.id.act_home_iv_home);
        ivHome.setColorFilter(getResources().getColor(R.color.colorMainBackground));

        ivMyCitizen = findViewById(R.id.act_home_iv_citizen);
        ivMyCitizen.setColorFilter(getResources().getColor(R.color.colorMainBackground));

        ivMyIncident = findViewById(R.id.act_home_iv_incident);
        ivMyIncident.setColorFilter(getResources().getColor(R.color.colorMainBackground));

        progressDialog = AppUtil.showLoaderDialog(activity, false);

        llHome.setOnClickListener(this);
        llMyCitizen.setOnClickListener(this);
        llMyIncident.setOnClickListener(this);

    }


    private void permission() {

        if (permissionsManager.hasAllPermissions(activity, mPermission)) {


            GPSTracker gpsTracker = new GPSTracker(activity);

            if (gpsTracker.canGetLocation()) {

                double latitude = gpsTracker.getLatitude();
                double longitude = gpsTracker.getLongitude();

                Log.e("Latitude", latitude + "");
                Log.e("longitude", longitude + "");


                llHome.setBackground(getResources().getDrawable(R.drawable.round_corner_status));
                llMyCitizen.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
                isMyVerifiend = true;
                homeFragment = new HomeFragment();
                loadFragment(homeFragment);

                getMyCitizenValue();


            } else {
                gpsTracker.showSettingsAlert();
            }

        } else {

            permissionsManager.requestPermissionsIfNecessaryForResult(activity, mPermission, new PermissionsResultAction() {
                @Override
                public void onGranted() {

                    GPSTracker gpsTracker = new GPSTracker(activity);
                    if (gpsTracker.canGetLocation()) {
                        double latitude = gpsTracker.getLatitude();
                        double longitude = gpsTracker.getLongitude();

                        Log.e("Latitude", latitude + "");
                        Log.e("longitude", longitude + "");


                        llHome.setBackground(getResources().getDrawable(R.drawable.round_corner_status));
                        llMyCitizen.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
                        isMyVerifiend = true;

                        homeFragment = new HomeFragment();
                        loadFragment(homeFragment);

                        getMyCitizenValue();

                    } else {
                        gpsTracker.showSettingsAlert();
                    }

                }

                @Override
                public void onDenied(String permission) {
                    //Toast.makeText(activity, "Please Allow Permission", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }


    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.change_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void getMyCitizenValue() {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_CITIZEN;
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                progressDialog.dismiss();
                Log.e("MyCitizenData", response + "");
                goldValue = 0;

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));

                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);

                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {

                        jsonArrayResponse = jsonObject.optJSONArray(KeyConstant.KEY_RESPONSE);

                        if (jsonArrayResponse != null) {
                            Log.e("length", jsonArrayResponse.length() + "");

                            if (jsonArrayResponse.length() > 0) {
                                for (int i = 0; i < jsonArrayResponse.length(); i++) {
                                    JSONObject object = jsonArrayResponse.optJSONObject(i);

                                    boolean isMain = object.optBoolean("isMain");
                                    Log.e("isMain", isMain + "");

                                    if (isMain) {
                                        goldValue = goldValue + 1;
                                    }

                                }
                            }


                            Log.e("goldValue", goldValue + "");


                        }

                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });


    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        if (homeFragment != null && homeFragment.isVisible()) {
            AppUtil.showCloseAppDialog(activity);

        } else if (myCitizenFragment != null && myCitizenFragment.isVisible()) {
            llHome.setBackground(getResources().getDrawable(R.drawable.round_corner_status));
            llMyCitizen.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
            ivHome.setColorFilter(getResources().getColor(R.color.colorMainBackground));
            actionButton.setVisibility(View.GONE);
            isIncident = false;
            isMyVerifiend = false;
            homeFragment = new HomeFragment();
            loadFragment(homeFragment);
        }else if (myIncidentFragment != null && myIncidentFragment.isVisible()) {
            llHome.setBackground(getResources().getDrawable(R.drawable.round_corner_status));
            llMyCitizen.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
            llMyIncident.setBackground(getResources().getDrawable(R.drawable.round_corner_grey_line));
            isIncident = false;
            isMyVerifiend = false;
            ivHome.setColorFilter(getResources().getColor(R.color.colorMainBackground));
            actionButton.setVisibility(View.GONE);

            homeFragment = new HomeFragment();
            loadFragment(homeFragment);
        }
    }
}
