package com.app.gotv.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.app.gotv.R;
import com.app.gotv.adapter.AdapterRegionsConstituencyBranch;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.runtimepermission.PermissionsManager;
import com.app.gotv.runtimepermission.PermissionsResultAction;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.utils.GPSTracker;
import com.app.gotv.vollysinglton.VolleyUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static com.app.gotv.constant.UrlConstant.IMAGE_TYPE_FIRST;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener{
    private Activity activity = SignUpActivity.this;

    private EditText etName = null, etContact = null, etPassword = null, etConfirmPassword = null, etUserName = null, etVoterId = null;
    private TextView tvDob = null, tvGender = null, tvRegion = null, tvConstituency = null, tvElectorialArea = null, tvBranchName = null;
    private ImageView ivImage = null;
    public static EditText etAddress = null;

    private Button btnCreateMmber = null;
    private int selectedPosition = -1;
    private Dialog dialogCameraGallery = null;
    public String[] mPermission = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    private PermissionsManager permissionsManager;
    private LinearLayout llCameraGallery = null;
    private String imageFilePath = "";
    private Bitmap imageBitmap;
    private Dialog progressDialog = null, dialogRegionConstituencyBranchName = null;
    private Uri fileUri;
    private int imageType;
    final private static int REQUEST_CODE_TAKE_IMAGE = 10;
    final private static int REQUEST_CODE_SELECT_IMAGE = 11;
    private String imageSelectedKey = null, strFileAttachForImage = null;
    private VolleyUtils volleyUtils = null;
    private boolean isRegions = false, isConstituency = false, isElectoral = false, isBranchName = false;
    private String idRegions = "", idConstituency = "", idElectoral = "", idBranchName = "", branchCode = "";
    private int mYear, mMonth, mDay, mHour, mMinute;
    private GoogleMap mMap;
    private GPSTracker gpsTracker = null;
    private CheckBox cbUseMyLocation = null, cbUseMap = null;
    public static double latitude = 0.0, longitude = 0.0;
    private Geocoder geocoder = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initWidgets();
        permissionsManager = PermissionsManager.getInstance();
        AWSMobileClient.getInstance().initialize(this).execute();
        volleyUtils = new VolleyUtils();
        geocoder = new Geocoder(this, Locale.getDefault());
        EnableGPSIfPossible();

    }

    @Override
    protected void onStart() {
        super.onStart();
        permission();

    }

    private void EnableGPSIfPossible()
    {
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
    }

    private void initWidgets() {
        etName = findViewById(R.id.act_signUp_et_name);
        etContact = findViewById(R.id.act_signUp_et_contact);
        etPassword = findViewById(R.id.act_signUp_et_password);
        etUserName = findViewById(R.id.act_signUp_et_userName);
        etVoterId = findViewById(R.id.act_signUp_et_voter_id);
        etAddress = findViewById(R.id.act_signUp_et_address);
        ivImage = findViewById(R.id.iv_image);
        etConfirmPassword = findViewById(R.id.act_signUp_et_confirm_pw);
        tvDob = findViewById(R.id.act_signUp_tv_dob);
        tvDob.setOnClickListener(this);
        tvGender = findViewById(R.id.act_signUp_tv_gender);
        tvGender.setOnClickListener(this);
        tvRegion = findViewById(R.id.act_signUp_tv_region);
        tvRegion.setOnClickListener(this);
        tvConstituency = findViewById(R.id.act_signUp_tv_constituency);
        tvConstituency.setOnClickListener(this);
        tvElectorialArea = findViewById(R.id.act_signUp_tv_electoral);
        tvElectorialArea.setOnClickListener(this);
        tvBranchName = findViewById(R.id.act_signUp_tv_branch_name);
        tvBranchName.setOnClickListener(this);

        btnCreateMmber = findViewById(R.id.act_signUp_btn_create_member);
        btnCreateMmber.setOnClickListener(this);
        progressDialog = AppUtil.showLoaderDialog(activity, false);

        llCameraGallery = findViewById(R.id.ll_camera_gallery);
        llCameraGallery.setOnClickListener(this);

        cbUseMyLocation = findViewById(R.id.cb_use_my_location);
        cbUseMap = findViewById(R.id.cb_use_map);
        cbUseMap.setOnClickListener(this);
        cbUseMyLocation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.act_signUp_tv_dob:
                final Calendar c = Calendar.getInstance();

                c.add(Calendar.YEAR, -18);
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String myFormat = "MM/dd/yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                        Log.e("year", year + "");
                        Log.e("monthOfYear", monthOfYear + "");
                        Log.e("dayOfMonth", dayOfMonth + "");

                        //etDob.setText(sdf.format(c.getTime()));
                        tvDob.setText(dayOfMonth + "/" + (monthOfYear + 1)+"/" + year);

                    }
                }, mYear, mMonth, mDay);
                c.set(mYear, mMonth, mDay);
                long value = c.getTimeInMillis();
                datePickerDialog.getDatePicker().setMaxDate(value);
                datePickerDialog.show();

               /* final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                // String year =

                                String myFormat = "MM/dd/yyyy"; //In which you need put here
                                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                                Log.e("year", year + "");
                                Log.e("monthOfYear", monthOfYear + "");
                                Log.e("dayOfMonth", dayOfMonth + "");

                                //etDob.setText(sdf.format(c.getTime()));
                                tvDob.setText(dayOfMonth + "/" + monthOfYear + "/" + year);

                            }


                        }, mYear - 18, mMonth, mDay);

                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());


                datePickerDialog.show();*/
                break;
            case R.id.act_signUp_tv_gender:
                showGenderDialog();

                break;
            case R.id.act_signUp_tv_region:
                isRegions = true;
                isConstituency = false;
                isElectoral = false;
                isBranchName = false;
                getRegionsData();
                break;
            case R.id.act_signUp_tv_constituency:
                isRegions = false;
                isConstituency = true;
                isBranchName = false;
                isElectoral = false;
                if (!idRegions.equalsIgnoreCase("")) {
                    getConstituencyData(idRegions);

                } else {
                    AppUtil.showCommonPopup(activity, getResources().getString(R.string.please_enter_region) + "");
                }
                break;
            case R.id.act_signUp_tv_electoral:
                isRegions = false;
                isConstituency = false;
                isBranchName = false;
                isElectoral = true;

                if (!idConstituency.equalsIgnoreCase("")) {
                    getElectoralData(idConstituency);

                } else {
                    AppUtil.showCommonPopup(activity, getResources().getString(R.string.please_enter_constituency) + "");
                }
                break;
            case R.id.act_signUp_tv_branch_name:
                isRegions = false;
                isConstituency = false;
                isElectoral = false;
                isBranchName = true;
                if (!idElectoral.equalsIgnoreCase("")) {
                    getBranchNameData(idElectoral);

                } else {
                    AppUtil.showCommonPopup(activity, getResources().getString(R.string.please_enter_branch_name) + "");
                }

                break;


            case R.id.ll_camera_gallery:
                showImageDialog(IMAGE_TYPE_FIRST);
                break;

            case R.id.act_signUp_btn_create_member:
                if (checkValidation()) {
                    progressDialog.show();
                    uploadeImageOnAws(new File(imageSelectedKey));

                }
                break;


            case R.id.cb_use_my_location:
                try {
                    cbUseMyLocation.setChecked(true);
                    cbUseMap.setChecked(false);
                    gpsTracker = new GPSTracker(activity);

                    latitude = gpsTracker.getLatitude();
                    longitude = gpsTracker.getLongitude();

                    List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                    String addressCurrent = addressList.get(0).getAddressLine(0);
                    etAddress.setText(addressCurrent);

                } catch (Exception e) {

                }


                break;

            case R.id.cb_use_map:
                cbUseMyLocation.setChecked(false);
                cbUseMap.setChecked(true);

                Intent intent = new Intent(activity, LocationSignUpActivity.class);
                startActivity(intent);

                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (dialogCameraGallery != null) {
                dialogCameraGallery.dismiss();

            }

            Uri selectedImage;
            Cursor cursor;
            String[] filePathColumn = {MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA};
            int columnIndex;

            switch (requestCode) {

                case REQUEST_CODE_SELECT_IMAGE:

                    selectedImage = data.getData();
                    cursor = activity.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);

                    if (imageType == IMAGE_TYPE_FIRST) {

                        imageFilePath = cursor.getString(columnIndex);
                        Log.e("imageFilePath", "" + imageFilePath);

                        Uri uri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", new File(imageFilePath));
                        try {

                            imageBitmap = handleSamplingAndRotationBitmap(activity, uri);
                            Log.e("galleryImageFile", new File(imageFilePath) + "");
                            byte[] imageGet = convertFileToByteArray(new File(imageFilePath));
                            Log.e("galleryImageByte", imageGet + "");

                            imageSelectedKey = getPath(activity, data.getData());
                            Log.e("imageSelectedKey", "" + imageSelectedKey);


                            if (imageGet != null) {
                                // UpdateImage(imageGet);
                                ivImage.setImageBitmap(imageBitmap);


                            } else {
                                Toast.makeText(activity, "Please Select Image once more", Toast.LENGTH_SHORT).show();
                            }


                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }

                    cursor.close();

                    break;

                case REQUEST_CODE_TAKE_IMAGE:


                    if (imageType == IMAGE_TYPE_FIRST) {


                        Uri uri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", new File(imageFilePath));
                        try {

                            imageBitmap = handleSamplingAndRotationBitmap(activity, uri);
                            Log.e("cameraImageFile", new File(imageFilePath) + "");
                            byte[] imageGet = convertFileToByteArray(new File(imageFilePath));
                            Log.e("cameraImageByte", imageGet + "");
                            imageSelectedKey = imageFilePath;
                            Log.e("imageSelectedKey", "" + imageSelectedKey);

                            if (imageGet != null) {
                                // UpdateImage(imageGet);
                                ivImage.setImageBitmap(imageBitmap);

                            } else {
                                Toast.makeText(activity, "Please Capture Image once more", Toast.LENGTH_SHORT).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    break;

            }
        }
    }

    private boolean checkValidation() {
        if (etName.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_full_name));
            return false;
        } else if (etUserName.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_user_name));
            return false;
        } else if (etContact.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_phone_number));
            return false;
        } else if (etPassword.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_password));
            return false;
        } else if (etPassword.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_password));
            return false;
        } else if (etConfirmPassword.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_confirm_password));
            return false;
        } else if (etConfirmPassword.getText().length() < 3) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_6_digit_password));
            return false;
        } else if (!etConfirmPassword.getText().toString().trim().equalsIgnoreCase(etPassword.getText().toString().trim())) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.password_does_not_match));
            return false;
        } else if (tvGender.getText().toString().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_select_gender));
            return false;
        } else if (tvDob.getText().toString().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_select_dob));
            return false;
        } else if (etVoterId.getText().toString().trim().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_voter_id));
            return false;
        } else if (idRegions.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_region));
            return false;
        } else if (idConstituency.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_constituency));
            return false;
        } else if (idElectoral.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_electoral));
            return false;
        } else if (idBranchName.equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_branch_name));
            return false;
        } else if (etAddress.getText().toString().equalsIgnoreCase("")) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_address));
            return false;
        } else if (imageSelectedKey == null) {
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_select_image));
            return false;
        }else if (gpsTracker == null){
            AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.location_issue));
            return false;
        }

        return true;

    }


    private void permission() {

        if (permissionsManager.hasAllPermissions(activity, mPermission)) {

            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE );
            boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            Log.e("Gps",statusOfGPS+"");
            gpsTracker = new GPSTracker(activity);
        } else {

            permissionsManager.requestPermissionsIfNecessaryForResult(activity, mPermission, new PermissionsResultAction() {
                @Override
                public void onGranted() {
                    gpsTracker = new GPSTracker(activity);


                }

                @Override
                public void onDenied(String permission) {

                    permission();

                }
            });
        }
    }

    private void showGenderDialog() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.gender_dialog);

        TextView tvMale = dialog.findViewById(R.id.popup_content);
        TextView tvFemale = dialog.findViewById(R.id.popup_content_description);

        tvMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tvGender.setText("" + getResources().getString(R.string.male));
                dialog.dismiss();

            }
        });

        tvFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tvGender.setText("" + getResources().getString(R.string.female));
                dialog.dismiss();

            }
        });


        dialog.show();
    }

    private void showImageDialog(final int imageType) {

        this.imageType = imageType;

        dialogCameraGallery = new Dialog(activity); // Context, this, etc.
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        dialogCameraGallery.setContentView(R.layout.dialog_choose_profile);
        dialogCameraGallery.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogCameraGallery.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialogCameraGallery.getWindow().setAttributes(lp);

        LinearLayout llCamera = dialogCameraGallery.findViewById(R.id.popup_camera);
        LinearLayout llGallery = dialogCameraGallery.findViewById(R.id.popup_gallery);


        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = 1;
                mediaIntent();
            }
        });


        llGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition = 0;
                mediaIntent();

            }
        });


        dialogCameraGallery.show();

    }


    private void mediaIntent() {

        if (selectedPosition == 0) {

            Intent intent2 = new Intent();
            intent2.setType("image/*");
            intent2.setAction(Intent.ACTION_PICK);
            startActivityForResult(Intent.createChooser(intent2, "Select Picture From Gallery"), REQUEST_CODE_SELECT_IMAGE);

        } else if (selectedPosition == 1) {

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            startActivityForResult(cameraIntent, REQUEST_CODE_TAKE_IMAGE);

        }
    }

    private void getRegionsData() {
        progressDialog.show();

        volleyUtils.GET_METHOD_WITH_REGION_CONSTITUENCY_BRANCH_NAME(activity, UrlConstant.URL_GET_REGIONS, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
                AppUtil.showCommonPopup(activity, message);
            }

            @Override
            public void onResponse(Object response) {
                Log.e("GetRegionsData", response + "");
                try {
                    JSONObject resp = new JSONObject(String.valueOf(response));
                    int status = resp.optInt(KeyConstant.KEY_STATUS);
                    final String message = resp.optString(KeyConstant.KEY_MESSAGE);

                    progressDialog.dismiss();

                    if (status == 200) {

                        JSONArray jsonArrayResponse = resp.optJSONArray(KeyConstant.KEY_RESPONSE);

                        showDataRegionsConstituenctBranchName(jsonArrayResponse);

                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    AppUtil.showCommonPopup(activity, e + "");

                }


            }
        });

    }

    private void getConstituencyData(String idRegions) {
        progressDialog.show();


        String url = UrlConstant.URL_GET_CONSTITUENCIES + "/" + idRegions;
        Log.e("url_constituenct", url + "");

        volleyUtils.GET_METHOD_WITH_REGION_CONSTITUENCY_BRANCH_NAME(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
                AppUtil.showCommonPopup(activity, message);
            }

            @Override
            public void onResponse(Object response) {
                Log.e("GetRegionsData", response + "");
                try {
                    JSONObject resp = new JSONObject(String.valueOf(response));
                    int status = resp.optInt(KeyConstant.KEY_STATUS);
                    final String message = resp.optString(KeyConstant.KEY_MESSAGE);

                    progressDialog.dismiss();

                    if (status == 200) {

                        JSONArray jsonArrayResponse = resp.optJSONArray(KeyConstant.KEY_RESPONSE);

                        showDataRegionsConstituenctBranchName(jsonArrayResponse);

                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void getElectoralData(String idConstituency) {
        progressDialog.show();


        String url = UrlConstant.URL_GET_ELECTORAL + "/" + idConstituency;
        Log.e("url_electoral", url + "");

        volleyUtils.GET_METHOD_WITH_REGION_CONSTITUENCY_BRANCH_NAME(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
                AppUtil.showCommonPopup(activity, message);
            }

            @Override
            public void onResponse(Object response) {
                Log.e("GetData", response + "");
                try {
                    JSONObject resp = new JSONObject(String.valueOf(response));
                    int status = resp.optInt(KeyConstant.KEY_STATUS);
                    final String message = resp.optString(KeyConstant.KEY_MESSAGE);

                    progressDialog.dismiss();

                    if (status == 200) {

                        JSONArray jsonArrayResponse = resp.optJSONArray(KeyConstant.KEY_RESPONSE);

                        showDataRegionsConstituenctBranchName(jsonArrayResponse);

                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void getBranchNameData(String idElectoral) {
        progressDialog.show();

        String url = UrlConstant.URL_GET_BRANCH_NAME + "/" + idElectoral;

        volleyUtils.GET_METHOD_WITH_REGION_CONSTITUENCY_BRANCH_NAME(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
                AppUtil.showCommonPopup(activity, message);
            }

            @Override
            public void onResponse(Object response) {
                try {
                    Log.e("BranchRes", response + "");
                    JSONObject resp = new JSONObject(String.valueOf(response));
                    int status = resp.optInt(KeyConstant.KEY_STATUS);
                    final String message = resp.optString(KeyConstant.KEY_MESSAGE);

                    progressDialog.dismiss();

                    if (status == 200) {

                        JSONArray jsonArrayResponse = resp.optJSONArray(KeyConstant.KEY_RESPONSE);

                        showDataRegionsConstituenctBranchName(jsonArrayResponse);

                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void showDataRegionsConstituenctBranchName(JSONArray jsonArray) {
        dialogRegionConstituencyBranchName = new Dialog(activity);
        dialogRegionConstituencyBranchName.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogRegionConstituencyBranchName.setCancelable(true);
        dialogRegionConstituencyBranchName.setContentView(R.layout.dialog_regions_constituency_branch);
        RecyclerView recyclerView = dialogRegionConstituencyBranchName.findViewById(R.id.rv_regions);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));

        AdapterRegionsConstituencyBranch adapterCategoryCreatAds = new AdapterRegionsConstituencyBranch(activity, jsonArray);
        recyclerView.setAdapter(adapterCategoryCreatAds);

        dialogRegionConstituencyBranchName.show();

    }

    public void setIdRegionsConstituencyBranch(int position, JSONArray jsonArray) {
        dialogRegionConstituencyBranchName.dismiss();

        JSONObject objectChild = jsonArray.optJSONObject(position);

        if (isRegions) {

            idRegions = objectChild.optString(KeyConstant.KEY_ID);
            String name = objectChild.optString(KeyConstant.KEY_NAME);
            tvRegion.setText("" + name);


            tvElectorialArea.setText("");
            tvConstituency.setText("");
            tvBranchName.setText("");
            idConstituency = "";
            idElectoral = "";
            idBranchName = "";

            Log.e("idRegions", idRegions + "");
            Log.e("RegionsName", name + "");

        } else if (isConstituency) {

            idConstituency = objectChild.optString(KeyConstant.KEY_ID);
            String name = objectChild.optString(KeyConstant.KEY_NAME);
            tvConstituency.setText("" + name);

            Log.e("idConstituency", idConstituency + "");
            Log.e("ConstituencyName", name + "");


        } else if (isElectoral) {

            idElectoral = objectChild.optString(KeyConstant.KEY_ID);
            String name = objectChild.optString(KeyConstant.KEY_NAME);
            tvElectorialArea.setText("" + name);

            Log.e("idElectoral", idElectoral + "");
            Log.e("ElectoralName", name + "");


        } else if (isBranchName) {

            idBranchName = objectChild.optString(KeyConstant.KEY_ID);
            String name = objectChild.optString(KeyConstant.KEY_NAME);
            branchCode = objectChild.optString(KeyConstant.KEY_BRANCH_CODE);

            tvBranchName.setText("" + name);


            Log.e("idBranchName", idBranchName + "");
            Log.e("BranchName", name + "");
            Log.e("branchCode", branchCode + "");


        }

    }

    public Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage) throws IOException {
        int MAX_HEIGHT = 512;
        int MAX_WIDTH = 512;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);

        return img;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);


            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;


            final float totalPixels = width * height;

            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = null;

        if (imageType == IMAGE_TYPE_FIRST) {

            ei = new ExifInterface(imageFilePath);
        }/* else if (imageType == IMAGE_TYPE_SECOND) {

            ei = new ExifInterface(secondImageFilePath);

        }*/

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public Uri getOutputMediaFileUri(int type) {

        Uri uri = null;

        if (imageType == IMAGE_TYPE_FIRST) {

            imageFilePath = getOutputMediaFile(type).getPath();
            Log.e("fileUriPath", imageFilePath);
            uri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".provider", new File(imageFilePath));

        }

        return uri;
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "/dow");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Hello Camera", "Oops! Failed create Hello Camera directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");
        return mediaFile;
    }

    public static byte[] convertFileToByteArray(File f) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(f);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 8];
            int bytesRead = 0;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArray;
    }

    public static String getPath(final Context context, final Uri uri) {


        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;


        // DocumentProvider

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

                // ExternalStorageProvider

                if (isExternalStorageDocument(uri)) {

                    final String docId = DocumentsContract.getDocumentId(uri);

                    final String[] split = docId.split(":");

                    final String type = split[0];


                    if ("primary" .equalsIgnoreCase(type)) {

                        return Environment.getExternalStorageDirectory() + "/" + split[1];

                    }


                }

                // DownloadsProvider

                else if (isDownloadsDocument(uri)) {


                    final String id = DocumentsContract.getDocumentId(uri);

                    final Uri contentUri = ContentUris.withAppendedId(

                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));


                    return getDataColumn(context, contentUri, null, null);

                }

                // MediaProvider

                else if (isMediaDocument(uri)) {

                    final String docId = DocumentsContract.getDocumentId(uri);

                    final String[] split = docId.split(":");

                    final String type = split[0];


                    Uri contentUri = null;

                    if ("image" .equals(type)) {

                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

                    } else if ("video" .equals(type)) {

                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

                    } else if ("audio" .equals(type)) {

                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

                    }


                    final String selection = "_id=?";

                    final String[] selectionArgs = new String[]{

                            split[1]

                    };


                    return getDataColumn(context, contentUri, selection, selectionArgs);

                }

            }

            // MediaStore (and general)

            else if ("content" .equalsIgnoreCase(uri.getScheme())) {

                return getDataColumn(context, uri, null, null);

            }

            // File

            else if ("file" .equalsIgnoreCase(uri.getScheme())) {

                return uri.getPath();

            }
        }


        return null;

    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {


        Cursor cursor = null;

        final String column = "_data";

        final String[] projection = {

                column

        };


        try {

            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,

                    null);

            if (cursor != null && cursor.moveToFirst()) {

                final int column_index = cursor.getColumnIndexOrThrow(column);

                return cursor.getString(column_index);

            }

        } finally {

            if (cursor != null)

                cursor.close();

        }

        return null;

    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */

    public static boolean isExternalStorageDocument(Uri uri) {

        return "com.android.externalstorage.documents" .equals(uri.getAuthority());

    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */

    public static boolean isDownloadsDocument(Uri uri) {

        return "com.android.providers.downloads.documents" .equals(uri.getAuthority());

    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */

    public static boolean isMediaDocument(Uri uri) {

        return "com.android.providers.media.documents" .equals(uri.getAuthority());

    }


    private void uploadeImageOnAws(File file) {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String curr = sdf.format(new Date());

        final String imageKey = curr + file.getName();

        strFileAttachForImage = imageKey;

        Log.e("imageKey", imageKey);

        Log.e("file", file.getPath());

        TransferUtility transferUtility =

                TransferUtility.builder()

                        .context(getApplicationContext())

                        .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())

                        .s3Client(new AmazonS3Client(AWSMobileClient.getInstance().getCredentialsProvider()))

                        .build();


        TransferObserver uploadObserver = transferUtility.upload(imageKey, file);


        uploadObserver.setTransferListener(new TransferListener() {


            @Override

            public void onStateChanged(int id, TransferState state) {

                if (TransferState.COMPLETED == state) {

                    String fullName = etName.getText().toString();
                    String password = etPassword.getText().toString();

                    String gender = tvGender.getText().toString();
                    String dob = tvDob.getText().toString();
                    String userName = etUserName.getText().toString();
                    String phoneNumber = etContact.getText().toString();
                    String address = etAddress.getText().toString();
                    String voterId = etVoterId.getText().toString();

                    String region = tvRegion.getText().toString();
                    String constituency = tvConstituency.getText().toString();
                    String branchName = tvBranchName.getText().toString();
                    String electorial = tvElectorialArea.getText().toString();

                    JSONObject jsonObject = new JSONObject();

                    JSONArray jsonArrayLocation = new JSONArray();

                    try {
                        jsonArrayLocation.put(gpsTracker.getLatitude());
                        jsonArrayLocation.put(gpsTracker.getLongitude());


                        jsonObject.put(KeyConstant.KEY_NAME, fullName);
                        jsonObject.put(KeyConstant.KEY_GENDER, gender);
                        jsonObject.put(KeyConstant.KEY_DOB, dob);
                        jsonObject.put(KeyConstant.KEY_EMAIL, userName);
                        jsonObject.put(KeyConstant.KEY_PASSWORD, password);
                        jsonObject.put(KeyConstant.KEY_LOCATION, jsonArrayLocation);

                        jsonObject.put(KeyConstant.KEY_CONTACT, phoneNumber);
                        jsonObject.put(KeyConstant.KEY_ADDRESS, address);
                        jsonObject.put(KeyConstant.KEY_VOTER_ID, voterId);
                        jsonObject.put(KeyConstant.KEY_REGION_ID, idRegions);
                        jsonObject.put(KeyConstant.KEY_CONSTITUENCY_ID, idConstituency);
                        jsonObject.put(KeyConstant.KEY_ELECTORAL_ID, idElectoral);
                        jsonObject.put(KeyConstant.KEY_BRANCH_ID, idBranchName);
                        jsonObject.put(KeyConstant.KEY_IMAGE, strFileAttachForImage);
                        jsonObject.put(KeyConstant.KEY_IS_APP, true);


                    } catch (Exception e) {
                        progressDialog.dismiss();

                        Log.e("DataCreateException", e + "");
                        Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                    }

                    Log.e("DataCreateObject", jsonObject + "");

                    addCitizenMember(jsonObject);


                }

            }


            @Override

            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;

                int percentDone = (int) percentDonef;


                Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");

            }


            @Override

            public void onError(int id, Exception ex) {

                Toast.makeText(activity, "" + ex, Toast.LENGTH_SHORT).show();


            }


        });

    }

    private void addCitizenMember(JSONObject jsonObject) {
        volleyUtils.POST_METHOD_WITH_JSON_SIGN_UP(activity, UrlConstant.URL_SIGN_UP, jsonObject, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
                AppUtil.showCommonPopup(activity, message);

            }

            @Override
            public void onResponse(Object response) {
                Log.e("SignUPResponse", response + "");
                progressDialog.dismiss();

                try {
                    JSONObject objectResponse = new JSONObject(String.valueOf(response));
                    int status = objectResponse.optInt(KeyConstant.KEY_STATUS);
                    String message = objectResponse.optString(KeyConstant.KEY_MESSAGE);
                    if (status == 200) {
                        //showCommonPopup(activity,message);
                        String sms = "Registered Successfully";
                        Toast.makeText(activity, "" + sms, Toast.LENGTH_LONG).show();
                        finish();


                    } else {
                        // showCommonPopup(activity,message);
                        Toast.makeText(activity, "" + message, Toast.LENGTH_LONG).show();

                        finish();


                    }

                } catch (Exception e) {

                }


            }
        });

    }


    private void turnGPSOn(){

        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        Log.e("asassa","prof");
        if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }

    private  void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Yout GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


}
