package com.app.gotv.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.gotv.R;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.model.AllCitizenModel;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.vollysinglton.VolleyUtils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Activity activity = DetailActivity.this;

    public TextView tvToolbar = null, tvFullName, tvGender, tvOccupation, tvPhone, tvVoterId, tvAddress, tvStatus, tvRegions, tvConstituency, tvElectoral, tvBranch, tvHasRefferal, tvRefferedThrough;
    public ImageView ivBack = null, ivImage, ivImageRefffered = null, ivAddRefferal = null;

    private Button btnStandBy = null, btnVoted = null;
    public TextView tvFullNameRefffered, tvGenderRefffered, tvOccupationRefffered, tvPhoneRefffered, tvVoterIdRefffered, tvAddressRefffered, tvStatusRefffered, tvRegionsRefffered, tvConstituencyRefffered, tvBranchRefffered, tvHasRefferalRefffered, tvRefferedThroughRefffered, tvRefferedByTo;
    public TextView tvBranchVerifiaction = null, tvVoterIDVerification = null, tvcitizenBranchVerification = null;
    private LinearLayout llReffered = null;
    private JSONObject jsonObj = null, objectReferredBy = null, objectReferredTo = null;
    private Dialog progressDialog = null;
    private String citizenId = null;
    private VolleyUtils volleyUtils = null;
    private TextView tvAdvocationFor = null;
    private TextView tvPositionStatus = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initWidgets();
        volleyUtils = new VolleyUtils();

        try {

            jsonObj = new JSONObject(getIntent().getStringExtra("citizenData"));

            Log.e("Detail", jsonObj + "");

            showData(jsonObj);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.btn_add_stand_by:
                JSONObject object = new JSONObject();
                try {
                    object.put(KeyConstant.KEY_STATUS, UrlConstant.STAND_BY);
                    //setUserVotedOrstandBy(object);

                    showMobileStatusDialog(activity, getResources().getString(R.string.change_status), object);

                    Log.e("StandBy", object + "");

                } catch (Exception e) {
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.btn_add_voted:

                JSONObject objectVoted = new JSONObject();
                try {
                    objectVoted.put(KeyConstant.KEY_STATUS, UrlConstant.VOTED);
                    //setUserVotedOrstandBy(objectVoted);
                    showMobileStatusDialog(activity, getResources().getString(R.string.change_status), objectVoted);

                    Log.e("VotedObject", objectVoted + "");

                } catch (Exception e) {
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();

                }
                break;
        }
    }


    private void initWidgets() {
        tvToolbar = findViewById(R.id.tv_toolbar);
        ivBack = findViewById(R.id.iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);

        tvFullName = findViewById(R.id.tv_home_name);
        tvGender = findViewById(R.id.tv_home_gender);
        tvOccupation = findViewById(R.id.tv_home_occupation);
        tvPhone = findViewById(R.id.tv_home_phone);
        tvVoterId = findViewById(R.id.tv_home_voter_id);
        tvAddress = findViewById(R.id.tv_home_address);
        tvStatus = findViewById(R.id.tv_home_status);
        tvRegions = findViewById(R.id.tv_home_regions);
        tvConstituency = findViewById(R.id.tv_home_constituency);
        tvBranch = findViewById(R.id.tv_home_branch);
        tvHasRefferal = findViewById(R.id.tv_home__has_referral);
        tvRefferedThrough = findViewById(R.id.tv_home_reffered_through);
        ivImage = findViewById(R.id.iv_home_image);

        tvAdvocationFor = findViewById(R.id.tv_home_advocating);
        tvPositionStatus = findViewById(R.id.tv_home_position_status);




      /*  tvRefferedByTo = findViewById(R.id.tv_referred);
        tvFullNameRefffered = findViewById(R.id.tv_detail_name);
        tvGenderRefffered = findViewById(R.id.tv_detail_gender);
        tvOccupationRefffered = findViewById(R.id.tv_detail_occupation);
        tvPhoneRefffered = findViewById(R.id.tv_detail_phone);
        tvVoterIdRefffered = findViewById(R.id.tv_detail_voter_id);
        tvAddressRefffered = findViewById(R.id.tv_detail_address);
        tvStatusRefffered = findViewById(R.id.tv_detail_status);
        tvRegionsRefffered = findViewById(R.id.tv_detail_regions);
        tvConstituencyRefffered = findViewById(R.id.tv_detail_constituency);
        tvBranchRefffered = findViewById(R.id.tv_detail_branch);
        tvHasRefferalRefffered = findViewById(R.id.tv_detail__has_referral);
        tvRefferedThroughRefffered = findViewById(R.id.tv_detail_reffered_through);
        ivImageRefffered = findViewById(R.id.iv_detail_image);
        llReffered = findViewById(R.id.ll_detail_parent);
        llReffered.setOnClickListener(this);*/

        tvElectoral = findViewById(R.id.tv_home_electrol);

        btnStandBy = findViewById(R.id.btn_add_stand_by);
        btnVoted = findViewById(R.id.btn_add_voted);

        btnStandBy.setOnClickListener(this);
        btnVoted.setOnClickListener(this);
        progressDialog = AppUtil.showLoaderDialog(activity, false);


        tvBranchVerifiaction = findViewById(R.id.tv_branch_verification);
        tvVoterIDVerification = findViewById(R.id.tv_voterId_verification);
        tvcitizenBranchVerification = findViewById(R.id.tv_citizrn_branch_verification);

    }

    private void showData(JSONObject object) {

        String imageUrl = object.optString(KeyConstant.KEY_IMAGE);
        String name = object.optString(KeyConstant.KEY_NAME);
        String gender = object.optString(KeyConstant.KEY_GENDER);
        String DOB = object.optString(KeyConstant.KEY_DOB);
        String occupation = object.optString(KeyConstant.KEY_OCCUPATION);
        final String contact = object.optString(KeyConstant.KEY_CONTACT);
        String voter_id = object.optString(KeyConstant.KEY_VOTER_ID);
        String address = object.optString(KeyConstant.KEY_ADDRESS);
        String status = object.optString(KeyConstant.KEY_STATUS);
        citizenId = object.optString(KeyConstant.KEY_ID);

        String possitionStatus = object.optString(KeyConstant.KEY_POSITION_DETAIL);


        if (possitionStatus.equalsIgnoreCase("0")) {
            tvPositionStatus.setText("EXECUTIVE");

        } else if (possitionStatus.equalsIgnoreCase("1")) {
            tvPositionStatus.setText("MEMBER");

        } else if (possitionStatus.equalsIgnoreCase("2")) {
            tvPositionStatus .setText("SYMPATISER");

        }


        boolean isBranchVerification = object.optBoolean("branch_varification");
        boolean isVoterIdVerification = object.optBoolean("voter_id_varification");
        boolean isCitizenBranchVerification = object.optBoolean("citizen_branch_varification");

        if (isBranchVerification) {

            tvBranchVerifiaction.setText("" + getResources().getText(R.string.verified));
            tvBranchVerifiaction.setTextColor(getResources().getColor(R.color.colorGreen));

        } else {
            tvBranchVerifiaction.setText("" + getResources().getText(R.string.not_verified));
            tvBranchVerifiaction.setTextColor(getResources().getColor(R.color.colorRed));


        }

        if (isVoterIdVerification) {

            tvVoterIDVerification.setText("" + getResources().getText(R.string.verified));
            tvVoterIDVerification.setTextColor(getResources().getColor(R.color.colorGreen));

        } else {
            tvVoterIDVerification.setText("" + getResources().getText(R.string.not_verified));
            tvVoterIDVerification.setTextColor(getResources().getColor(R.color.colorRed));

        }

        if (isCitizenBranchVerification) {

            tvcitizenBranchVerification.setText("" + getResources().getText(R.string.verified));
            tvcitizenBranchVerification.setTextColor(getResources().getColor(R.color.colorGreen));

        } else {
            tvcitizenBranchVerification.setText("" + getResources().getText(R.string.not_verified));
            tvcitizenBranchVerification.setTextColor(getResources().getColor(R.color.colorRed));

        }


        JSONObject jsonObjectAdvocating = object.optJSONObject(KeyConstant.KEY_ADVOCATING);
        String description = jsonObjectAdvocating.optString(KeyConstant.KEY_DESCRIPTION);
        tvAdvocationFor.setText(description);




        tvToolbar.setText("" + activity.getResources().getString(R.string.citizen_detail));

        tvFullName.setText("" + name);
        tvGender.setText("" + gender);
        tvOccupation.setText("" + occupation);
        tvPhone.setText("" + contact);
        tvVoterId.setText("" + voter_id);
        tvAddress.setText("" + address);

        if (status.equalsIgnoreCase("1")) {
            tvStatus.setText("STAND BY");


        } else if (status.equalsIgnoreCase("2")) {
            tvStatus.setText("VOTED");

        }

        JSONObject objectRegions = object.optJSONObject(KeyConstant.KEY_REGION);
        if (objectRegions != null) {
            String regionsName = objectRegions.optString(KeyConstant.KEY_NAME);
            String regionsId = objectRegions.optString(KeyConstant.KEY_ID);
            tvRegions.setText("" + regionsName);

        }


        JSONObject objectConstituency = object.optJSONObject(KeyConstant.KEY_CONSTITUENCY);
        if (objectConstituency != null) {
            String constituencyName = objectConstituency.optString(KeyConstant.KEY_NAME);
            String constituencyId = objectConstituency.optString(KeyConstant.KEY_ID);
            tvConstituency.setText("" + constituencyName);

        }

        JSONObject objectElectoral = object.optJSONObject(KeyConstant.KEY_ELECTORAL_AREA);
        if (objectElectoral != null) {
            String constituencyName = objectElectoral.optString(KeyConstant.KEY_NAME);
            tvElectoral.setText("" + constituencyName);

        }


        JSONObject objectBranch = object.optJSONObject(KeyConstant.KEY_BRANCH_NAME);
        if (objectBranch != null) {
            String branchName = objectBranch.optString(KeyConstant.KEY_NAME);
            String branchId = objectBranch.optString(KeyConstant.KEY_ID);
            tvBranch.setText("" + branchName);

        }


     /*   Picasso.get()
                .load(imageUrl)
                .resize(100, 100)
                .rotate(90)
                .centerCrop()
                .into(ivImage);*/


        Glide.with(DetailActivity.this)
                .load(imageUrl)
                .placeholder(R.mipmap.app_icon)
                .error(R.mipmap.app_icon)
                .into(ivImage);


        objectReferredBy = object.optJSONObject(KeyConstant.KEY_REFERRED_BY);
        objectReferredTo = object.optJSONObject(KeyConstant.KEY_REFERRED_TO);


       /* if (objectReferredBy != null) {
            ivAddRefferal.setVisibility(View.INVISIBLE);
            tvRefferedByTo.setVisibility(View.VISIBLE);
            tvRefferedByTo.setText("" + activity.getResources().getString(R.string.referred_by));
            llReffered.setVisibility(View.VISIBLE);

            JSONObject objectRegionsReferredBy = objectReferredBy.optJSONObject(KeyConstant.KEY_REGION);
            if (objectRegionsReferredBy != null) {
                String regionsNameReferredBy = objectRegionsReferredBy.optString(KeyConstant.KEY_NAME);
                String regionsIdReferredBy = objectRegionsReferredBy.optString(KeyConstant.KEY_ID);
                tvRegionsRefffered.setText("" + regionsNameReferredBy);

            }


            JSONObject objectConstituencyReferredBy = objectReferredBy.optJSONObject(KeyConstant.KEY_CONSTITUENCY);
            if (objectConstituencyReferredBy != null) {
                String constituencyNameReferredBy = objectConstituencyReferredBy.optString(KeyConstant.KEY_NAME);
                String constituencyIdReferredBy = objectConstituencyReferredBy.optString(KeyConstant.KEY_ID);
                tvConstituencyRefffered.setText("" + constituencyNameReferredBy);


            }

            JSONObject objectBranchReferredBy = objectReferredBy.optJSONObject(KeyConstant.KEY_BRANCH_NAME);
            if (objectBranchReferredBy != null) {
                String branchNameReferredBy = objectBranchReferredBy.optString(KeyConstant.KEY_NAME);
                String branchIdReferredBy = objectBranchReferredBy.optString(KeyConstant.KEY_ID);
                tvBranchRefffered.setText("" + branchNameReferredBy);

            }


            String imageUrlReffered = objectReferredBy.optString(KeyConstant.KEY_IMAGE);
            String nameReffered = objectReferredBy.optString(KeyConstant.KEY_NAME);
            String genderReffered = objectReferredBy.optString(KeyConstant.KEY_GENDER);
            String DOBReffered = objectReferredBy.optString(KeyConstant.KEY_DOB);
            String occupationReffered = objectReferredBy.optString(KeyConstant.KEY_OCCUPATION);
            String contactReffered = objectReferredBy.optString(KeyConstant.KEY_CONTACT);
            String voter_idReffered = objectReferredBy.optString(KeyConstant.KEY_VOTER_ID);
            String addressReffered = objectReferredBy.optString(KeyConstant.KEY_ADDRESS);
            String statusReffered = objectReferredBy.optString(KeyConstant.KEY_STATUS);


            tvFullNameRefffered.setText("" + nameReffered);
            tvGenderRefffered.setText("" + genderReffered);
            tvOccupationRefffered.setText("" + occupationReffered);
            tvPhoneRefffered.setText("" + contactReffered);
            tvVoterIdRefffered.setText("" + voter_idReffered);
            tvAddressRefffered.setText("" + addressReffered);

            if (statusReffered.equalsIgnoreCase("1")) {
                tvStatusRefffered.setText("STAND BY");


            } else if (statusReffered.equalsIgnoreCase("2")) {
                tvStatusRefffered.setText("VOTED");

            }


            Picasso.get()
                    .load(imageUrlReffered)
                    .resize(100, 100)
                    .centerCrop()
                    .into(ivImageRefffered);


        } else {
            ivAddRefferal.setVisibility(View.VISIBLE);
            tvRefferedByTo.setVisibility(View.VISIBLE);
            tvRefferedByTo.setText("" + activity.getResources().getString(R.string.refferal));
            llReffered.setVisibility(View.GONE);

        }*/

        /*if (objectReferredTo != null) {
            ivAddRefferal.setVisibility(View.INVISIBLE);
            tvRefferedByTo.setVisibility(View.VISIBLE);
            tvRefferedByTo.setText("" + activity.getResources().getString(R.string.referred_to));
            llReffered.setVisibility(View.VISIBLE);


            JSONObject objectRegionsReferredTo = objectReferredTo.optJSONObject(KeyConstant.KEY_REGION);
            String regionsNameReferredTo = objectRegionsReferredTo.optString(KeyConstant.KEY_NAME);
            String regionsIdReferredTo = objectRegionsReferredTo.optString(KeyConstant.KEY_ID);


            JSONObject objectConstituencyReferredTo = objectReferredTo.optJSONObject(KeyConstant.KEY_CONSTITUENCY);
            String constituencyNameReferredTo = objectConstituencyReferredTo.optString(KeyConstant.KEY_NAME);
            String constituencyIdReferredTo = objectConstituencyReferredTo.optString(KeyConstant.KEY_ID);

            JSONObject objectBranchReferredTo = objectReferredTo.optJSONObject(KeyConstant.KEY_BRANCH_NAME);
            String branchNameReferredTo = objectBranchReferredTo.optString(KeyConstant.KEY_NAME);
            String branchIdReferredTo = objectBranchReferredTo.optString(KeyConstant.KEY_ID);

            tvRegionsRefffered.setText("" + regionsNameReferredTo);
            tvConstituencyRefffered.setText("" + constituencyNameReferredTo);
            tvBranchRefffered.setText("" + branchNameReferredTo);

            String imageUrlReffered = objectReferredTo.optString(KeyConstant.KEY_IMAGE);
            String nameReffered = objectReferredTo.optString(KeyConstant.KEY_NAME);
            String genderReffered = objectReferredTo.optString(KeyConstant.KEY_GENDER);
            String DOBReffered = objectReferredTo.optString(KeyConstant.KEY_DOB);
            String occupationReffered = objectReferredTo.optString(KeyConstant.KEY_OCCUPATION);
            String contactReffered = objectReferredTo.optString(KeyConstant.KEY_CONTACT);
            String voter_idReffered = objectReferredTo.optString(KeyConstant.KEY_VOTER_ID);
            String addressReffered = objectReferredTo.optString(KeyConstant.KEY_ADDRESS);
            String statusReffered = objectReferredTo.optString(KeyConstant.KEY_STATUS);


            tvFullNameRefffered.setText("" + nameReffered);
            tvGenderRefffered.setText("" + genderReffered);
            tvOccupationRefffered.setText("" + occupationReffered);
            tvPhoneRefffered.setText("" + contactReffered);
            tvVoterIdRefffered.setText("" + voter_idReffered);
            tvAddressRefffered.setText("" + addressReffered);

            if (statusReffered.equalsIgnoreCase("1")) {
                tvStatusRefffered.setText("STAND BY");


            } else if (statusReffered.equalsIgnoreCase("2")) {
                tvStatusRefffered.setText("VOTED");

            }


            Picasso.get()
                    .load(imageUrlReffered)
                    .resize(100, 100)
                    .centerCrop()
                    .into(ivImageRefffered);


        }*/
    }


    private void setUserVotedOrstandBy(JSONObject jsonObject) {
        progressDialog.show();

        volleyUtils.PUT_METHOD_WITH_JSON(activity, UrlConstant.URL_PUT_VOTED_OR_STAND_BY_CITIZEN + "?citizenId=" + citizenId, jsonObject, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
                AppUtil.showCommonPopup(activity, message);

            }

            @Override
            public void onResponse(Object response) {
                Log.e("AddVotedStandResponse", response + "");
                progressDialog.dismiss();

                try {
                    JSONObject objectResponse = new JSONObject(String.valueOf(response));
                    int status = objectResponse.optInt(KeyConstant.KEY_STATUS);
                    String message = objectResponse.optString(KeyConstant.KEY_MESSAGE);
                    if (status == 200) {
                        //showCommonPopup(activity,message);
                        Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();
                        finish();


                    } else {
                        // showCommonPopup(activity,message);
                        Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();

                        finish();


                    }

                } catch (Exception e) {

                }


            }
        });


    }

    private void showMobileStatusDialog(Context activity, String sms, final JSONObject object) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.common_dialog);
        // Window window = dialog.getWindow();
        // window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        TextView tvSms = dialog.findViewById(R.id.popup_content);
        TextView btnNo = dialog.findViewById(R.id.popup_no_btn);
        TextView btnYes = dialog.findViewById(R.id.popup_yes_btn);

        tvSms.setText("" + sms);

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserVotedOrstandBy(object);
                dialog.cancel();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();

            }
        });


        dialog.show();


    }


}
