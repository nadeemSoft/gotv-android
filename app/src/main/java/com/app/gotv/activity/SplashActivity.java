package com.app.gotv.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.app.gotv.R;
import com.app.gotv.sessionManagement.AppSession;
import com.app.gotv.utils.AppUtil;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity {
    private Activity activity = SplashActivity.this;
    private Handler mWaitHandler = new Handler();
    private AppSession appSession = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        appSession = AppSession.getInstance(activity);

        if (AppUtil.isNetworkAvailable(activity)) {
            mWaitHandler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (AppUtil.isNetworkAvailable(activity)) {
                        try {
                            if (appSession.getUserLogin()) {
                                Intent intent = new Intent(activity,HomeActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(activity,LoginActivity.class);
                                startActivity(intent);
                                finish();

                            }





                        } catch (Exception ignored) {
                            ignored.printStackTrace();
                            Log.e("Exception",ignored+"");
                        }
                    } else {
                        showLanguageDialog();
                        //Toast.makeText(activity, "" + getResources().getString(R.string.please_check_internet), Toast.LENGTH_LONG).show();
                    }

                }
            }, 5000);  // Give a 5 seconds delay.
        } else {
            showLanguageDialog();
            //Toast.makeText(activity, "" + getResources().getString(R.string.please_check_internet), Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mWaitHandler.removeCallbacksAndMessages(null);
    }



    public void showLanguageDialog() {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_dialog);

        TextView tvSms = dialog.findViewById(R.id.popup_content);
        TextView tvDescription = dialog.findViewById(R.id.popup_content_description);
        tvDescription.setVisibility(View.VISIBLE);

        TextView btnNo = dialog.findViewById(R.id.popup_no_btn);
        TextView btnYes = dialog.findViewById(R.id.popup_yes_btn);
        btnNo.setVisibility(View.GONE);
        btnYes.setVisibility(View.VISIBLE);

        tvSms.setText("" + "You're offline");
        tvDescription.setText("" + "Check your internet connection and try again");

        btnYes.setText("Try Again");

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtil.isNetworkAvailable(activity)) {
                    try {
                        Intent intent = new Intent(activity, SplashActivity.class);
                        startActivity(intent);
                        finish();


                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                    }
                } else {
                    //showLanguageDialog(activity);
                    Toast.makeText(activity, "" + getResources().getString(R.string.please_check_internet), Toast.LENGTH_LONG).show();
                }
            }
        });


        dialog.show();


    }


}
