package com.app.gotv.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.app.gotv.R;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.utils.DateConverter;
import com.app.gotv.vollysinglton.VolleyUtils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class IncidentDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Activity activity = IncidentDetailActivity.this;

    public TextView tvToolbar = null, tvHeadline, tvDescription, tvCategory = null, tvSubCategory = null, tvBranchCode = null, tvRegions, tvConstituency, tvElectoral, tvBranch, tvDate;
    public ImageView ivBack = null, ivImage;

    private Dialog progressDialog = null;
    private String incidentId = null, imageUrlVideoURl = null, imageUrl = null;
    private FrameLayout framePlayIcon = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incident_detail);
        initWidgets();

        try {

            incidentId = getIntent().getStringExtra("incidentID");

            Log.e("incidentId", incidentId + "");


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        getIncidentDeatil();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;

            case R.id.frame_play:
                if (imageUrlVideoURl != null) {
                    Intent intent = new Intent(activity, VideoActivity.class);
                    intent.putExtra("videoUrl", imageUrlVideoURl);
                    startActivity(intent);

                    //playVideoDialog(imageUrlVideoURl);

                }
                break;

            case R.id.iv_home_image:
                if (imageUrl != null) {
                    Intent intent = new Intent(activity, ImageFullScreenActivity.class);
                    intent.putExtra("imageUrl", imageUrl);
                    startActivity(intent);

                } else {
                    Toast.makeText(activity, "null", Toast.LENGTH_SHORT).show();
                }
                break;


        }
    }


    private void initWidgets() {
        tvToolbar = findViewById(R.id.tv_toolbar);
        ivBack = findViewById(R.id.iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);

        tvToolbar.setText(activity.getResources().getText(R.string.incident_detail));

        tvHeadline = findViewById(R.id.act_inc_detail_headline);
        tvDescription = findViewById(R.id.act_inc_detail_description);
        tvCategory = findViewById(R.id.act_inc_detail_category);
        tvSubCategory = findViewById(R.id.act_inc_detail_sub_category);
        tvRegions = findViewById(R.id.tv_home_regions);
        tvConstituency = findViewById(R.id.tv_home_constituency);
        tvBranch = findViewById(R.id.tv_home_branch);
        ivImage = findViewById(R.id.iv_home_image);
        ivImage.setOnClickListener(this);
        tvElectoral = findViewById(R.id.tv_home_electrol);
        tvDate = findViewById(R.id.tv_home_date);
        framePlayIcon = findViewById(R.id.frame_play);
        tvBranchCode = findViewById(R.id.tv_home_branch_code);
        framePlayIcon.setOnClickListener(this);

        progressDialog = AppUtil.showLoaderDialog(activity, false);


    }

    private void getIncidentDeatil() {
        progressDialog.show();

        VolleyUtils volleyUtils = new VolleyUtils();
        String url = UrlConstant.URL_GET_ALL_MY_CITIZEN + "/" + incidentId;
        volleyUtils.GET_METHOD(activity, url, new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
            }

            @Override
            public void onResponse(Object response) {
                progressDialog.dismiss();
                Log.e("IncidentData", response + "");

                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(response));
                    int status = jsonObject.optInt(KeyConstant.KEY_STATUS);
                    String message = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                    if (status == 200) {

                        JSONObject objectResponse = jsonObject.optJSONObject(KeyConstant.KEY_RESPONSE);

                        if (objectResponse != null) {

                            updateUI(objectResponse);

                        } else {
                            String sms = activity.getResources().getText(R.string.no_citizen).toString();
                            //AppUtil.showCommonPopup(activity, sms);


                        }


                    } else {
                        AppUtil.showCommonPopup(activity, message);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    private void updateUI(JSONObject object) {

        JSONObject objectMedia = object.optJSONObject("media");
        imageUrl = objectMedia.optString(KeyConstant.KEY_IMAGE);


        try {
            if (objectMedia.getString(KeyConstant.KEY_VIDEO) != null) {
                imageUrlVideoURl = objectMedia.getString(KeyConstant.KEY_VIDEO);
                framePlayIcon.setVisibility(View.VISIBLE);

            } else {
                framePlayIcon.setVisibility(View.GONE);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

/*
        Picasso.get()
                .load(imageUrl)
                .placeholder(R.mipmap.ic_launcher)
                .resize(100, 100)
                .centerCrop()
                .into(ivImage);*/


        Glide.with(IncidentDetailActivity.this)
                .load(imageUrl)
                .placeholder(R.mipmap.app_icon)
                .error(R.mipmap.app_icon)
                .into(ivImage);


        String headline = object.optString(KeyConstant.KEY_HEADLINE);
        String description = object.optString(KeyConstant.KEY_DESCRIPTION);

        String updateDate = object.optString(KeyConstant.KEY_UPDATE_DATE);


        tvHeadline.setText("" + headline);
        tvDescription.setText("" + description);

        JSONArray arrCategory = object.optJSONArray(KeyConstant.KEY_CATEGORY);
        JSONObject objectSubMainCate = arrCategory.optJSONObject(0);

        tvSubCategory.setText(objectSubMainCate.optString(KeyConstant.KEY_NAME));
        tvCategory.setText(objectSubMainCate.optJSONObject("category_id").optString(KeyConstant.KEY_NAME));

        JSONObject objectRegions = object.optJSONObject(KeyConstant.KEY_REGION);
        if (objectRegions != null) {
            String regionsName = objectRegions.optString(KeyConstant.KEY_NAME);
            String regionsId = objectRegions.optString(KeyConstant.KEY_ID);
            tvRegions.setText(regionsName + "");

        }


        JSONObject objectElectorial = object.optJSONObject(KeyConstant.KEY_ELECTORAL_AREA);
        if (objectRegions != null) {
            String electorailName = objectElectorial.optString(KeyConstant.KEY_NAME);
            String electorialId = objectElectorial.optString(KeyConstant.KEY_ID);
            tvElectoral.setText(electorailName + "");

        }

        JSONObject objectConstituency = object.optJSONObject(KeyConstant.KEY_CONSTITUENCY);

        if (objectConstituency != null) {
            String constituencyName = objectConstituency.optString(KeyConstant.KEY_NAME);
            String constituencyId = objectConstituency.optString(KeyConstant.KEY_ID);
            tvConstituency.setText(constituencyName + "");

        }

        JSONObject objectBranch = object.optJSONObject(KeyConstant.KEY_BRANCH);

        if (objectBranch != null) {
            String branchName = objectBranch.optString(KeyConstant.KEY_NAME);
            String branchId = objectBranch.optString(KeyConstant.KEY_ID);
            String branchCode = objectBranch.optString(KeyConstant.KEY_BRANCH_CODE);
            tvBranch.setText(branchName + "");
            tvBranchCode.setText(branchCode + "");
        }


        try {
            String dataNew = AppUtil.getDate(DateConverter.UTCToLocal(updateDate));
            String output = dataNew.substring(0, 10);

            SimpleDateFormat month_date = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(output);
            String month_name = month_date.format(date);

            tvDate.setText("" + dataNew);

        } catch (Exception e) {
            Log.e("ExceptionDate", e + "");
        }

    }


    private void imageDialog(String url) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        /*Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);*/
        dialog.setContentView(R.layout.custom_dialog_play_video);

        ImageView imageView = dialog.findViewById(R.id.image_dialog);

        imageView.setImageURI(Uri.parse(url));

      /*  Picasso.get()
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);*/


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        imageView.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        imageView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        imageView.setAdjustViewBounds(false);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        dialog.show();

    }

}
