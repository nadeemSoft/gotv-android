package com.app.gotv.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toolbar;

import com.app.gotv.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

public class ImageFullScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_full_screen);
        ImageView imageView = findViewById(R.id.image_dialog);
        String url = getIntent().getStringExtra("imageUrl");

        /*Picasso.get()
                .load(url)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);*/

        Glide.with(ImageFullScreenActivity.this)
                .load(url)
                .placeholder(R.mipmap.app_icon)
                .into(imageView);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        imageView.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
        imageView.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
        imageView.setAdjustViewBounds(false);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
    }
}
