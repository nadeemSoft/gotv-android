package com.app.gotv.activity;


import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.app.gotv.R;
import com.app.gotv.sessionManagement.AppSession;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.utils.GPSTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class LocationSignUpActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, View.OnClickListener, GoogleMap.OnCameraIdleListener {

    private AppCompatActivity activity;
    private GoogleMap mMap;

    private static final String LOG_TAG = "Main";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private TextView mAutocompleteTextView;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    private ImageView im_close;
    private LatLng userAddLatLng;
    private Dialog progressDialog = null;
    private TextView tvTitle = null;
    private ImageView ivBack = null;
    private Geocoder geocoder = null;
    private int request_code = 1001;
    private MarkerOptions markerOptions;
    private Marker resMarker;
    private Button btnOk;
    private LatLng latLng;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private Button btncenter;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private AppSession appSession = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appSession = AppSession.getInstance(activity);

        setContentView(R.layout.activity_location);
        activity = LocationSignUpActivity.this;
        GPSTracker gpsTracker = new GPSTracker(activity);

        geocoder = new Geocoder(this, Locale.getDefault());

        userAddLatLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());


        Log.e("lat", ">>>>" + userAddLatLng.latitude);
        Log.e("lng", ">>>>" + userAddLatLng.longitude);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        initViews();

        try {
            List<Address> addressList = geocoder.getFromLocation(userAddLatLng.latitude, userAddLatLng.longitude, 1);
            String addressCurrent = addressList.get(0).getAddressLine(0);
            mAutocompleteTextView.setText("" + addressCurrent);
            SignUpActivity.etAddress.setText(addressCurrent);

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("gecoder", e + "");
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.autocomplete:
                showLocation();
                break;

            case R.id.btn_ok:
                finish();
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setLangRecreate(String language) {

        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        if (language.equalsIgnoreCase("hi")) {

            //getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            Log.e("hindi", language);


        } else if (language.equalsIgnoreCase("en")) {

            // getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            Log.e("ENGLISH", language);


        }
    }


    private void initViews() {
        tvTitle = findViewById(R.id.tv_toolbar);
        tvTitle.setText(getResources().getText(R.string.location));
        ivBack = findViewById(R.id.iv_back);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setOnClickListener(this);
        btnOk = findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(this);
        progressDialog = AppUtil.showLoaderDialog(activity, false);


        initilizeMap();


        //Places.initialize(getApplicationContext(), getResources().getString(R.string.google_maps_key));


        mAutocompleteTextView = findViewById(R.id.autocomplete);
        mAutocompleteTextView.setOnClickListener(this);

        im_close = findViewById(R.id.act_loc_im_close);
        im_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAutocompleteTextView.setText("");

            }
        });

        //showLocation();


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setOnCameraIdleListener(this);
        CameraUpdate center;
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);
        mMap.moveCamera(zoom);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000); // two minute interval
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);


        if (userAddLatLng == null) {

            center = CameraUpdateFactory.newLatLng(new LatLng(22.7196, 75.8577));
            //resMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(22.7196, 75.8577)));

            mMap.moveCamera(center);
        } else {

            center = CameraUpdateFactory.newLatLng(userAddLatLng);
            //resMarker = mMap.addMarker(new MarkerOptions().position(userAddLatLng));

            mMap.moveCamera(center);

            // mMap.setMaxZoomPreference(20);
        }

        // mMap.setTrafficEnabled(true);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            mMap.setMyLocationEnabled(true);
        }

    }


    @Override
    public void onConnected(Bundle bundle) {
        //mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        // mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }


    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                Log.e("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;


                //Place current location marker
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Current Position");
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));


                //move map camera
            }
        }
    };


    private void initilizeMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(activity,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }


/*
    public void runGeocodeAPI(String latLng, String key) {

        if (AppUtil.isNetworkAvailable(activity)) {
            progressDialog.show();

            new GetLocationTask(activity, latLng, key, new AsyncCallback() {
                @Override
                public void setResponse(Integer responseCode, String responseStr) {

                    if (responseStr != null) {
                        try {
                            final JSONObject resp = new JSONObject(responseStr);
                            final JSONArray jsonArrayResult = resp.optJSONArray("results");


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();


                                    if (jsonArrayResult.length() > 0) {
                                        JSONObject jsonObject = jsonArrayResult.optJSONObject(0);
                                        String formatted_address = jsonObject.optString("formatted_address");
                                        mAutocompleteTextView.setText(formatted_address);
                                        CreateAdsActivity.tvLocation.setText(formatted_address);

                                    } else {
                                        String smsError = resp.optString("error_message");
                                        //Toast.makeText(activity, "" + smsError, Toast.LENGTH_LONG).show();
                                        AppUtil.showCommonPopup(activity, smsError);

                                    }


                                    // Toast.makeText(activity, "" + message, Toast.LENGTH_SHORT).show();

                                }
                            });


                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void setException(String e) {


                }
            }).execute();

        } else {
            //Toast.makeText(activity, "" + getResources().getString(R.string.please_check_internet), Toast.LENGTH_SHORT).show();
            AppUtil.showCommonPopup(activity, getResources().getString(R.string.please_check_internet).toString());

        }


    }
*/


    private void showLocation() {

        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(activity);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }

    }

    /*protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(activity, data);
                Log.e("placeName", place.getName() + "");
                //sessionManagement.setLatLog(latLng.latitude, latLng.longitude);

                try {
                  *//*  lat = latLng.latitude;
                    log = latLng.longitude;*//*
                    *//*addressesNewList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

                    String addressNew = addressesNewList.get(0).getAddressLine(0);
                    String city = addressesNewList.get(0).getLocality();
                    String state = addressesNewList.get(0).getAdminArea();
                    String country = addressesNewList.get(0).getCountryName();
                    String postalCode = addressesNewList.get(0).getPostalCode();
                    String knownName = addressesNewList.get(0).getFeatureName();*//*

                    mAutocompleteTextView.setText(place.getName());
                    CreateAdsActivity.tvLocation.setText(place.getName());
                    String location = place.toString();

                    System.out.println("manishshis" + location);

                    Log.e("Address", place.getAddress().toString());
                    Log.e("Name", place.getName().toString());


                    userAddLatLng = place.getLatLng();


                    CameraUpdate center = CameraUpdateFactory.newLatLng(userAddLatLng);

                    mMap.animateCamera(center);

                    CreateAdsActivity.latitude = userAddLatLng.latitude;
                    CreateAdsActivity.longitude = userAddLatLng.longitude;


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("placeExc", e + "");
                }


            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(activity, data);
                // TODO: Handle the error.
                Log.e("pl", status.getStatusMessage() + "");

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }



        *//*if (requestCode == request_code) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.e("ManishPlace: ", place.getName() + ", " + place.getId());

                mAutocompleteTextView.setText(place.getName());
                CreateAdsActivity.tvLocation.setText(place.getName());
                String location = place.toString();

                System.out.println("manishshis" + location);

                Log.e("Address", place.getAddress().toString());
                Log.e("Name", place.getName().toString());


                userAddLatLng = place.getLatLng();


                CameraUpdate center = CameraUpdateFactory.newLatLng(userAddLatLng);

                mMap.animateCamera(center);


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e("", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }*//*
    }*/

    @Override
    public void onCameraIdle() {
        LatLng centerLatLong = mMap.getCameraPosition().target;
        Log.e("MapIdle", centerLatLong + "");
        SignUpActivity.latitude = centerLatLong.latitude;
        SignUpActivity.longitude = centerLatLong.longitude;


        List<Address> addressList = null;
        try {
            addressList = geocoder.getFromLocation(centerLatLong.latitude, centerLatLong.longitude, 1);

            if (addressList.size() != 0) {
                String addressCurrent = addressList.get(0).getAddressLine(0);
                Log.e("addressChange", addressCurrent + "");
                mAutocompleteTextView.setText("" + addressCurrent);
                SignUpActivity.etAddress.setText("" + addressCurrent);


            }


        } catch (IOException e) {
            e.printStackTrace();
            Log.e("CameraIDelEx", e + "");
        }

        //runGeocodeAPI(centerLatLong.latitude + "," + centerLatLong.longitude, getResources().getString(R.string.google_maps_key));


    }


    private class GeocoderTask extends AsyncTask<String, Void, List<Address>> {

        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(activity);
            List<Address> addresses = null;

            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return addresses;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {

            if (addresses == null || addresses.size() == 0) {
                Toast.makeText(activity, "No Location found", Toast.LENGTH_SHORT).show();
            }

            // Clears all the existing markers on the map
            mMap.clear();

            // Adding Markers on Google Map for each matching address
            for (int i = 0; i < addresses.size(); i++) {

                Address address = addresses.get(i);

                System.out.println("svsfssfjfjf " + address);
                // Creating an instance of GeoPoint, to display in Google Map
                latLng = new LatLng(address.getLatitude(), address.getLongitude());

                System.out.println("aadtetsetse " + address.getAddressLine(0));
                markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(address.getAddressLine(0));
                mMap.addMarker(markerOptions);

                // Locate the first location
                if (i == 0)
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng)      // Sets the center of the map to Mountain View
                        .zoom(17)                   // Sets the zoom
                        .bearing(90)                // Sets the orientation of the camera to east
                        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    }




}