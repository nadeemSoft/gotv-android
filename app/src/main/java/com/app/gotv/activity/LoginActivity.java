package com.app.gotv.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.gotv.R;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.constant.UrlConstant;
import com.app.gotv.interfaces.VolleyResponseListener;
import com.app.gotv.sessionManagement.AppSession;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.vollysinglton.VolleyUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Activity activity = LoginActivity.this;
    private CardView cdLogin = null;
    private EditText etEmail = null, etPassword = null;
    private Dialog progressDialog = null;
    private AppSession appSession = null;
    private LinearLayout llSignUp = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initWidgets();
        appSession = AppSession.getInstance(activity);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_view_btn_login:
                if (etEmail.getText().toString().equalsIgnoreCase("")) {
                    AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_user_name));

                } else if (etPassword.getText().toString().trim().equalsIgnoreCase("")) {
                    AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_enter_password));

                } else if (etPassword.getText().length() < 3) {
                    AppUtil.showCommonPopup(activity, activity.getResources().getString(R.string.please_6_digit_password));
                } else {
                    userLogin(etEmail.getText().toString().trim(), etPassword.getText().toString().trim());


                }
                break;

            case R.id.ll_not_a_member_signUp:
                Intent intent = new Intent(activity, SignUpActivity.class);
                startActivity(intent);
                break;
        }

    }

    private void initWidgets() {
        etEmail = findViewById(R.id.et_email_login);
        etPassword = findViewById(R.id.et_password_login);
        llSignUp = findViewById(R.id.ll_not_a_member_signUp);
        llSignUp.setOnClickListener(this);
        progressDialog = AppUtil.showLoaderDialog(activity, false);
        cdLogin = findViewById(R.id.card_view_btn_login);
        cdLogin.setOnClickListener(this);
    }


    private void userLogin(String userEmail, String userPassword) {
        progressDialog.show();
        VolleyUtils volleyUtils = new VolleyUtils();

        volleyUtils.POST_METHOD(activity, true, UrlConstant.URL_LOGIN, getParams(userEmail, userPassword), new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(String.valueOf(message));

                    String messagessMS = jsonObject.optString(KeyConstant.KEY_MESSAGE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Toast.makeText(activity, "" + message, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onResponse(Object response) {
                if (response != null) {
                    progressDialog.dismiss();
                    Log.e("LoginResponse", response + "");
                    try {

                        JSONObject jsonObject = new JSONObject(String.valueOf(response));
                        String accessToken = jsonObject.optString(KeyConstant.KEY_ACCESS_TOKEN);
                        String refreshToken = jsonObject.optString(KeyConstant.KEY_REFRESH_TOKEN);
                        appSession.setToken(accessToken, refreshToken);

                        JSONObject objectUser = jsonObject.optJSONObject(KeyConstant.KEY_USER);
                        String name = objectUser.optString(KeyConstant.KEY_NAME);
                        String image = objectUser.optString(KeyConstant.KEY_IMAGE);
                        boolean isApproved = objectUser.optBoolean("isApproved");

                        boolean isFirtLogin = objectUser.optBoolean(KeyConstant.KEY_FIRST_LOGIN);
                        appSession.setMyName(name);
                        appSession.setUserImage(image);


                        JSONObject objectRegionId = objectUser.optJSONObject(KeyConstant.KEY_REGION_ID);
                        String regionId = objectRegionId.optString(KeyConstant.KEY_ID);
                        String nameRegion = objectRegionId.optString(KeyConstant.KEY_NAME);

                        appSession.saveRegionData(objectRegionId);

                        JSONObject objectConstituencyId = objectUser.optJSONObject(KeyConstant.KEY_CONSTITUENCY_ID);
                        String constituencyId = objectConstituencyId.optString(KeyConstant.KEY_ID);
                        String nameConstituency = objectConstituencyId.optString(KeyConstant.KEY_NAME);

                        appSession.saveConstituencyData(objectConstituencyId);

                        JSONObject objectElectorialId = objectUser.optJSONObject(KeyConstant.KEY_ELECTORAL_ID);
                        String electorailId = objectElectorialId.optString(KeyConstant.KEY_ID);
                        String nameElectorial = objectElectorialId.optString(KeyConstant.KEY_NAME);

                        appSession.saveElectorailData(objectElectorialId);

                        JSONObject objectBranchId = objectUser.optJSONObject(KeyConstant.KEY_BRANCH_ID);
                        String branchId = objectBranchId.optString(KeyConstant.KEY_ID);
                        String nameBranch = objectBranchId.optString(KeyConstant.KEY_NAME);

                        appSession.saveBranchData(objectBranchId);

                        if (!isFirtLogin) {
                            setUserPassword();

                        } else {
                            appSession.setUserLogin(true);

                            Intent intent = new Intent(activity, HomeActivity.class);
                            startActivity(intent);
                            finish();

                        }

                    } catch (Exception e) {
                        progressDialog.dismiss();

                        Toast.makeText(activity, "Login " + e, Toast.LENGTH_SHORT).show();
                    }


                }

            }
        });


    }

    public Map<String, String> getParams(String userEmail, String userPassword) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(KeyConstant.KEY_GRANT_TYPE, "password");
        params.put(KeyConstant.KEY_USER_NAME, userEmail);
        params.put(KeyConstant.KEY_PASSWORD, userPassword);
        params.put(KeyConstant.KEY_SCOPE, "2");
        Log.e("Params", params + "");

        return params;
    }


    public Map<String, String> getParamsResetPassword(String password) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(KeyConstant.KEY_PASSWORD, password);
        Log.e("ParamsReset", params + "");

        return params;
    }


    private void setUserPassword() {

        final Dialog dialog1 = new Dialog(activity);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.reset_password_dialog);
        dialog1.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog1.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog1.getWindow().setAttributes(lp);

        final EditText etNewPassword = dialog1.findViewById(R.id.et_new_password);
        final EditText etConfirmPassword = dialog1.findViewById(R.id.et_confirm_password);
        TextView tvConfirm = dialog1.findViewById(R.id.popup_yes_btn);


        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNewPassword.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(activity, "" + activity.getResources().getString(R.string.please_enter_new_password), Toast.LENGTH_SHORT).show();

                } else if (etConfirmPassword.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(activity, "" + activity.getResources().getString(R.string.please_enter_confirm), Toast.LENGTH_SHORT).show();

                } else if (!etNewPassword.getText().toString().trim().equalsIgnoreCase(etConfirmPassword.getText().toString().trim())) {
                    Toast.makeText(activity, "" + activity.getResources().getString(R.string.password_does_not_match), Toast.LENGTH_SHORT).show();

                } else {
                    setPassword(etNewPassword.getText().toString(), dialog1);
                }
            }
        });


        dialog1.show();

    }

    private void setPassword(String strNewPassword, final Dialog dialog) {
        progressDialog.show();
        VolleyUtils volleyUtils = new VolleyUtils();

        volleyUtils.POST_METHOD(activity, false, UrlConstant.URL_POST_RESET_PASSWORD, getParamsResetPassword(strNewPassword), new VolleyResponseListener() {
            @Override
            public void onError(String message) {
                progressDialog.dismiss();

                Toast.makeText(activity, "" + message, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onResponse(Object response) {
                if (response != null) {
                    progressDialog.dismiss();
                    Log.e("resetResponse", response + "");
                    try {

                        dialog.dismiss();

                        appSession.setUserLogin(true);

                        Intent intent = new Intent(activity, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    } catch (Exception e) {
                        progressDialog.dismiss();

                        Toast.makeText(activity, "Reset Password " + e, Toast.LENGTH_SHORT).show();
                    }


                }

            }
        });

    }
}
