package com.app.gotv.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.app.gotv.R;

public class VideoActivity extends AppCompatActivity {
   private Activity activity = VideoActivity.this;
    private VideoView myVideoView;
    private int position = 0;
    private ProgressDialog progressDialog;
    private MediaController mediaControls;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set the main layout of the activity
        setContentView(R.layout.activity_video);

        myVideoView = findViewById(R.id.videoView1);

        String url = getIntent().getStringExtra("videoUrl");
        Log.e("urlVideo", url + "");

        //set the media controller buttons
        if (mediaControls == null) {
            mediaControls = new MediaController(activity);
        }


        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("Incident Video");
        progressDialog.setMessage("Video is loading Please waite...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {
            myVideoView.setMediaController(mediaControls);

            myVideoView.setVideoURI(Uri.parse(url));

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        myVideoView.requestFocus();
        //we also set an setOnPreparedListener in order to know when the video file is ready for playback
        myVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {
                // close the progress bar and play the video
                progressDialog.dismiss();
                Toast.makeText(activity, "Playing...", Toast.LENGTH_LONG).show();

                myVideoView.seekTo(position);
                if (position == 0) {
                    myVideoView.start();
                } else {
                    //if we come from a resumed activity, video playback will be paused
                    myVideoView.pause();
                }
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //we use onSaveInstanceState in order to store the video playback position for orientation change
        savedInstanceState.putInt("Position", myVideoView.getCurrentPosition());
        myVideoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //we use onRestoreInstanceState in order to play the video playback from the stored position
        position = savedInstanceState.getInt("Position");
        myVideoView.seekTo(position);
    }

}
   /* private Activity activity = VideoActivity.this;
    private VideoView videoView = null;
    ProgressDialog progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        videoView = findViewById(R.id.videoView1);

        progressBar =  new ProgressDialog(activity);
        progressBar.setMessage("Video is loading Please waite...");
        progressBar.setCancelable(false);
        progressBar.show();

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videoView.setLayoutParams(params);

        String url = getIntent().getStringExtra("videoUrl");

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        Log.e("urlVideo", url + "");


        try {
            Uri uri = Uri.parse(url);

            videoView.setMediaController(mediaController);
            videoView.setVideoURI(uri);
            videoView.requestFocus();
            //videoView.start();

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    Toast.makeText(activity, "Playing...", Toast.LENGTH_LONG).show();
                    progressBar.dismiss();
                    mp.setLooping(true);
                    videoView.start();
                }
            });


            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Log.e("onCompletion", mp + "");
                    videoView.pause();

                    finish();
                }
            });

            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Log.e("video", "setOnErrorListener ");
                    finish();

                    return true;

                }
            });

        } catch (Exception e) {
            Log.e("EXCEPTION", e + "");
        }

    }
*/


