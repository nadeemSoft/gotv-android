package com.app.gotv.viewholder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.gotv.R;

public class HomeAdapterViewHolder2 extends RecyclerView.ViewHolder {
    private Context context;
    public LinearLayout llParent;
    public TextView tvFullName ,tvSex,tvVoterId ,tvAge, tvStatus,tvPollingStation;

    public HomeAdapterViewHolder2(LayoutInflater inflater, ViewGroup parent, Context context) {
        super(inflater.inflate(R.layout.adapter_home2, parent, false));
        this.context = context;

        llParent = itemView.findViewById(R.id.ll_adapter_home_parent);

        tvFullName = itemView.findViewById(R.id.tv_home_name);
        tvSex =  itemView.findViewById(R.id.tv_home_sex);
        tvVoterId = itemView.findViewById(R.id.tv_home_voter_id);
        tvStatus = itemView.findViewById(R.id.tv_home_status);
        tvAge = itemView.findViewById(R.id.tv_home_age);

        tvPollingStation =itemView.findViewById(R.id.tv_home_polling_station);



    }
}
