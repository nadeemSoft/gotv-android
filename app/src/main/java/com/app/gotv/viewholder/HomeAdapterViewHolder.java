package com.app.gotv.viewholder;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.gotv.R;

public class HomeAdapterViewHolder extends RecyclerView.ViewHolder {
    private Context context;
    public LinearLayout llParent;
    public TextView tvFullName ,tvGender,tvOccupation,tvPhone,tvVoterId , tvAddress,tvStatus,tvRegions,tvConstituency,tvBranch, tvHasRefferal,tvRefferedThrough;
    public ImageView ivImage;

    public HomeAdapterViewHolder(LayoutInflater inflater, ViewGroup parent, Context context) {
        super(inflater.inflate(R.layout.adapter_home, parent, false));
        this.context = context;

        llParent = itemView.findViewById(R.id.ll_adapter_home_parent);

        tvFullName = itemView.findViewById(R.id.tv_home_name);
        tvGender =  itemView.findViewById(R.id.tv_home_gender);
        tvOccupation = itemView.findViewById(R.id.tv_home_occupation);
        tvPhone = itemView.findViewById(R.id.tv_home_phone);
        tvVoterId = itemView.findViewById(R.id.tv_home_voter_id);
        tvAddress = itemView.findViewById(R.id.tv_home_address);
        tvStatus = itemView.findViewById(R.id.tv_home_status);
        tvRegions = itemView.findViewById(R.id.tv_home_regions);
        tvConstituency = itemView.findViewById(R.id.tv_home_constituency);
        tvBranch = itemView.findViewById(R.id.tv_home_branch);
        tvHasRefferal = itemView.findViewById(R.id.tv_home__has_referral);
        tvRefferedThrough = itemView.findViewById(R.id.tv_home_reffered_through);
        ivImage = itemView.findViewById(R.id.iv_home_image);



    }
}
