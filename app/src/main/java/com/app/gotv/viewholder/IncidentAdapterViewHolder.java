package com.app.gotv.viewholder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.gotv.R;

public class IncidentAdapterViewHolder extends RecyclerView.ViewHolder {
    private Context context;
    public LinearLayout llParent;
    public TextView tvHeadline ,tvDescription,tvRegions,tvConstituency,tvBranch,tvElectorial,tvDate,tvCategory;
    public ImageView ivImage;
    public FrameLayout frameLayout;

    public IncidentAdapterViewHolder(LayoutInflater inflater, ViewGroup parent, Context context) {
        super(inflater.inflate(R.layout.adapter_incident, parent, false));
        this.context = context;

        llParent = itemView.findViewById(R.id.ll_adapter_home_parent);

        tvHeadline = itemView.findViewById(R.id.tv_incident_headline);
        tvDescription =  itemView.findViewById(R.id.tv_incident_description);
        tvRegions = itemView.findViewById(R.id.tv_home_regions);
        tvConstituency = itemView.findViewById(R.id.tv_home_constituency);
        tvBranch = itemView.findViewById(R.id.tv_home_branch);
        tvElectorial = itemView.findViewById(R.id.tv_home_electorial);
        ivImage = itemView.findViewById(R.id.iv_incident_image);
        frameLayout  = itemView.findViewById(R.id.frame_play);
        tvCategory = itemView.findViewById(R.id.tv_incident_category);

        tvDate = itemView.findViewById(R.id.tv_home_Date);


    }
}
