package com.app.gotv.viewholder;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.gotv.R;

public class RegionsConstituencyBranchAdapterViewHolder extends  RecyclerView.ViewHolder {
    private Context context;
    public TextView tvdata;
    public View viewLine;

    public RegionsConstituencyBranchAdapterViewHolder(LayoutInflater inflater, ViewGroup parent, Context context) {
        super(inflater.inflate(R.layout.adapter_data_regions_constituency_branch, parent, false));
        this.context = context;

        tvdata = itemView.findViewById(R.id.tv_data);
        viewLine = itemView.findViewById(R.id.view_line);

    }
}
