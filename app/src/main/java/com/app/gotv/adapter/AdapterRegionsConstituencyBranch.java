package com.app.gotv.adapter;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.gotv.activity.AddCitizenActivity;
import com.app.gotv.activity.AddIncidentActivity;
import com.app.gotv.activity.AddRefferalActivity;
import com.app.gotv.activity.SignUpActivity;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.viewholder.RegionsConstituencyBranchAdapterViewHolder;

import org.json.JSONArray;
import org.json.JSONObject;

public class AdapterRegionsConstituencyBranch extends RecyclerView.Adapter<RegionsConstituencyBranchAdapterViewHolder> {
    private Activity mActivity;
    private JSONArray jsonArray;
    private String language;

    public AdapterRegionsConstituencyBranch(Activity activity, JSONArray jsonArray) {
        this.mActivity = activity;
        this.jsonArray = jsonArray;
    }

    @NonNull
    @Override
    public RegionsConstituencyBranchAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new RegionsConstituencyBranchAdapterViewHolder(LayoutInflater.from(viewGroup.getContext()), viewGroup, mActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull RegionsConstituencyBranchAdapterViewHolder holder, final int i) {
        JSONObject object = jsonArray.optJSONObject(i);

        if (mActivity instanceof AddCitizenActivity){
            String name = object.optString(KeyConstant.KEY_DESCRIPTION);
            holder.tvdata.setText(""+name);

        }else {
            String name = object.optString(KeyConstant.KEY_NAME);
            holder.tvdata.setText(""+name);
        }



        holder.tvdata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity instanceof AddCitizenActivity){
                    ((AddCitizenActivity)mActivity).setIdRegionsConstituencyBranch(i,jsonArray);

                }else if (mActivity instanceof AddRefferalActivity){
                    ((AddRefferalActivity)mActivity).setIdRegionsConstituencyBranch(i,jsonArray);

                }else if (mActivity instanceof SignUpActivity){
                    ((SignUpActivity)mActivity).setIdRegionsConstituencyBranch(i,jsonArray);

                }else if (mActivity instanceof AddIncidentActivity){
                    ((AddIncidentActivity)mActivity).setIdRegionsConstituencyBranch(i,jsonArray);

                }
            }
        });


        if (i == jsonArray.length()-1){
            holder.viewLine.setVisibility(View.GONE);

        }

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }
}
