package com.app.gotv.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.gotv.R;
import com.app.gotv.activity.AddCitizenActivity;
import com.app.gotv.activity.DetailActivity;
import com.app.gotv.activity.HomeActivity;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.model.AllCitizenModel;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.utils.DateConverter;
import com.app.gotv.viewholder.HomeAdapterViewHolder;
import com.app.gotv.viewholder.HomeAdapterViewHolder2;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class HomeAdapter2 extends RecyclerView.Adapter<HomeAdapterViewHolder2> {
    private Context context;
    private ArrayList<AllCitizenModel> modelArrayList;

    public HomeAdapter2(Context context, ArrayList<AllCitizenModel> modelArrayList) {
        this.context = context;
        this.modelArrayList = modelArrayList;


    }

    @NonNull
    @Override
    public HomeAdapterViewHolder2 onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new HomeAdapterViewHolder2(LayoutInflater.from(viewGroup.getContext()), viewGroup, context);

    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapterViewHolder2 holder, final int i) {
        AllCitizenModel citizenModel = modelArrayList.get(i);

        String name = citizenModel.getName();
        String pollingStation = citizenModel.getPolling_station();
        String sex = citizenModel.getSex();
        String voter_id = citizenModel.getVoter_id();

       // String status = object.optString(KeyConstant.KEY_STATUS);
        int age = citizenModel.getAge();


        holder.tvFullName.setText("" + name);
        holder.tvSex.setText("" + sex);
        holder.tvVoterId.setText("" + voter_id);
        holder.tvAge.setText("" + age);
        holder.tvPollingStation.setText("" + pollingStation);

        /*if (status.equalsIgnoreCase("1")) {
            holder.tvStatus.setText("Stand By");


        } else if (status.equalsIgnoreCase("2")) {
            holder.tvStatus.setText("Voted");

        }*/


        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("goldValueAdapter",HomeActivity.goldValue+"");

                if (HomeActivity.goldValue >= 5) {
                    AppUtil.showCommonPopup(context, context.getResources().getString(R.string.not_Add_citizen));


                } else {
                    AllCitizenModel citizenModel = modelArrayList.get(i);

                    Intent intent = new Intent(context, AddCitizenActivity.class);
                    intent.putExtra("data",citizenModel);
                    context.startActivity(intent);

                }


            }
        });


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }
}
