package com.app.gotv.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.gotv.R;
import com.app.gotv.activity.DetailActivity;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.model.AllCitizenModel;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.utils.DateConverter;
import com.app.gotv.viewholder.HomeAdapterViewHolder;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class MyCitizenAdapter extends RecyclerView.Adapter<HomeAdapterViewHolder> {
    private Context context;
    private JSONArray jsonArrayResponse;

    public MyCitizenAdapter(Context context, JSONArray jsonArrayResponse) {
        this.context = context;
        this.jsonArrayResponse = jsonArrayResponse;


    }

    @NonNull
    @Override
    public HomeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new HomeAdapterViewHolder(LayoutInflater.from(viewGroup.getContext()), viewGroup, context);

    }

    @Override
    public void onBindViewHolder(@NonNull HomeAdapterViewHolder holder, final int i) {
        JSONObject object = jsonArrayResponse.optJSONObject(i);

        String imageUrl = object.optString(KeyConstant.KEY_IMAGE);
        String name = object.optString(KeyConstant.KEY_NAME);
        String gender = object.optString(KeyConstant.KEY_GENDER);
        String DOB = object.optString(KeyConstant.KEY_DOB);
        String occupation = object.optString(KeyConstant.KEY_OCCUPATION);
        final String contact = object.optString(KeyConstant.KEY_CONTACT);
        String voter_id = object.optString(KeyConstant.KEY_VOTER_ID);
        String address = object.optString(KeyConstant.KEY_ADDRESS);
        String status = object.optString(KeyConstant.KEY_STATUS);


       /* holder.tvFullName.setText("Name -> "+name);
        holder.tvGender.setText("Gender -> "+gender);
        holder.tvOccupation.setText("Occupation -> "+occupation);
        holder.tvPhone.setText("Phone Number -> "+contact);
        holder.tvVoterId.setText("Voter Id -> "+voter_id);
        holder.tvAddress.setText("Address -> "+address);
*/

        holder.tvFullName.setText("" + name);
        holder.tvGender.setText("G" + gender);
        holder.tvOccupation.setText("" + occupation);
        holder.tvPhone.setText("" + contact);
        holder.tvVoterId.setText("" + voter_id);
        holder.tvAddress.setText("" + address);


        if (status.equalsIgnoreCase("1")) {
            holder.tvStatus.setText("Stand By");


        } else if (status.equalsIgnoreCase("2")) {
            holder.tvStatus.setText("Voted");

        }


        JSONObject objectRegions = object.optJSONObject(KeyConstant.KEY_REGION);
        if (objectRegions != null) {
            String regionsName = objectRegions.optString(KeyConstant.KEY_NAME);
            String regionsId = objectRegions.optString(KeyConstant.KEY_ID);
            holder.tvRegions.setText(regionsName + "");

        }

        JSONObject objectConstituency = object.optJSONObject(KeyConstant.KEY_CONSTITUENCY);

        if (objectConstituency != null) {
            String constituencyName = objectConstituency.optString(KeyConstant.KEY_NAME);
            String constituencyId = objectConstituency.optString(KeyConstant.KEY_ID);
            holder.tvConstituency.setText(constituencyName + "");

        }

        JSONObject objectBranch = object.optJSONObject(KeyConstant.KEY_BRANCH_NAME);


        if (objectBranch != null) {
            String branchName = objectBranch.optString(KeyConstant.KEY_NAME);
            String branchId = objectBranch.optString(KeyConstant.KEY_ID);
            holder.tvBranch.setText(branchName + "");
        }






       /* try {
            String dataNew = AppUtil.getDate(DateConverter.UTCToLocal(DOB));
            String output = dataNew.substring(0, 10);

            SimpleDateFormat month_date = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(output);
            String month_name = month_date.format(date);
            //holder.t.setText("" + "  " + month_name);

        } catch (Exception e) {
            Log.e("ExceptionDate", e + "");
        }
*/
/*
        Picasso.get()
                .load(imageUrl)
                .resize(100, 100)
                .rotate(90)
                .centerCrop()
                .into(holder.ivImage);*/

        Glide.with(context)
                .load(imageUrl)
                .placeholder(R.mipmap.app_icon)
                .error(R.mipmap.app_icon)
                .into(holder.ivImage);


        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject object = jsonArrayResponse.optJSONObject(i);
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("citizenData", object.toString());
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return jsonArrayResponse.length();
    }
}
