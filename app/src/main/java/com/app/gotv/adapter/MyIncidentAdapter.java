package com.app.gotv.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.gotv.R;
import com.app.gotv.activity.DetailActivity;
import com.app.gotv.activity.IncidentDetailActivity;
import com.app.gotv.constant.KeyConstant;
import com.app.gotv.utils.AppUtil;
import com.app.gotv.utils.DateConverter;
import com.app.gotv.viewholder.IncidentAdapterViewHolder;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class MyIncidentAdapter extends RecyclerView.Adapter<IncidentAdapterViewHolder> {
    private Context context;
    private JSONArray jsonArrayResponse;

    public MyIncidentAdapter(Context context, JSONArray jsonArrayResponse) {
        this.context = context;
        this.jsonArrayResponse = jsonArrayResponse;


    }

    @NonNull
    @Override
    public IncidentAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new IncidentAdapterViewHolder(LayoutInflater.from(viewGroup.getContext()), viewGroup, context);

    }

    @Override
    public void onBindViewHolder(@NonNull IncidentAdapterViewHolder holder, final int i) {
        JSONObject object = jsonArrayResponse.optJSONObject(i);

        JSONObject objectMedia = object.optJSONObject("media");

        String imageUrl = objectMedia.optString(KeyConstant.KEY_IMAGE);
        try {
            if (objectMedia.getString(KeyConstant.KEY_VIDEO) != null){
                String videoUrl = objectMedia.getString(KeyConstant.KEY_VIDEO);
                holder.frameLayout.setVisibility(View.VISIBLE);

            }else {
                holder.frameLayout.setVisibility(View.GONE);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


      /*  Picasso.get()
                .load(imageUrl)
                .placeholder(R.mipmap.ic_launcher)
                .resize(100, 100)
                .centerCrop()
                .into(holder.ivImage);*/


        Glide.with(context)
                .load(imageUrl)
                .placeholder(R.mipmap.app_icon)
                .error(R.mipmap.app_icon)
                .into(holder.ivImage);



        String headline = object.optString(KeyConstant.KEY_HEADLINE);
        String description = object.optString(KeyConstant.KEY_DESCRIPTION);

        String updateDate = object.optString(KeyConstant.KEY_UPDATE_DATE);


        holder.tvHeadline.setText("" + headline);
        holder.tvDescription.setText(""+ description);

        JSONArray arrCategory = object.optJSONArray(KeyConstant.KEY_CATEGORY);

        if (arrCategory.length() > 0){
            holder.tvCategory.setText(arrCategory.optJSONObject(0).optString(KeyConstant.KEY_NAME));

        }


        JSONObject objectRegions = object.optJSONObject(KeyConstant.KEY_REGION);
        if (objectRegions != null) {
            String regionsName = objectRegions.optString(KeyConstant.KEY_NAME);
            String regionsId = objectRegions.optString(KeyConstant.KEY_ID);
            holder.tvRegions.setText(regionsName + "");

        }


        JSONObject objectElectorial = object.optJSONObject(KeyConstant.KEY_ELECTORAL_AREA);
        if (objectRegions != null) {
            String electorailName = objectElectorial.optString(KeyConstant.KEY_NAME);
            String electorialId = objectElectorial.optString(KeyConstant.KEY_ID);
            holder.tvElectorial.setText(electorailName + "");

        }

        JSONObject objectConstituency = object.optJSONObject(KeyConstant.KEY_CONSTITUENCY);

        if (objectConstituency != null) {
            String constituencyName = objectConstituency.optString(KeyConstant.KEY_NAME);
            String constituencyId = objectConstituency.optString(KeyConstant.KEY_ID);
            holder.tvConstituency.setText(constituencyName + "");

        }

        JSONObject objectBranch = object.optJSONObject(KeyConstant.KEY_BRANCH);

        if (objectBranch != null) {
            String branchName = objectBranch.optString(KeyConstant.KEY_NAME);
            String branchId = objectBranch.optString(KeyConstant.KEY_ID);
            holder.tvBranch.setText(branchName + "");
        }


        try {
            String dataNew = AppUtil.getDate(DateConverter.UTCToLocal(updateDate));
            Log.e("dataNew", dataNew + "");

            String output = dataNew.substring(0, 10);

            SimpleDateFormat month_date = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse(output);
            String month_name = month_date.format(date);

            holder.tvDate.setText(""+dataNew);

        } catch (Exception e) {
            Log.e("ExceptionDate", e + "");
        }



        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject object = jsonArrayResponse.optJSONObject(i);
                String id = object.optString(KeyConstant.KEY_ID);

                Intent intent = new Intent(context, IncidentDetailActivity.class);
                intent.putExtra("incidentID", id);

                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return jsonArrayResponse.length();
    }
}
