package com.app.gotv.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class DateConverter {


    public static String UTCToLocalInbox(String date) {

        try {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));


            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


            dateFormat.setTimeZone(TimeZone.getDefault());


            date = dateFormat.format(utcFormat.parse(date));


        } catch (ParseException e) {

            Log.e("UTC>>Local>>", ">>" + e.getMessage());

        }

        return date;

    }


    public static String UTCToLocalEdit(String date) {

        try {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));


            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


            dateFormat.setTimeZone(TimeZone.getDefault());


            if (date != null)
            date = dateFormat.format(utcFormat.parse(date));


        } catch (ParseException e) {

            Log.e("UTC>>Local>>", ">>" + e.getMessage());

        }

        return date;

    }


    public static String UTCToLocal(String date) {
        //2018-12-12T07:13:27.890Z

        try {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            //"2018-10-13 14:17:19"

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            DateFormat timeFormat = new SimpleDateFormat("hh:mm a");


            dateFormat.setTimeZone(TimeZone.getDefault());

            if (date != null)
                date = dateFormat.format(utcFormat.parse(date));


        } catch (ParseException e) {

            Log.e("UTC>>Local>>", ">>" + e.getMessage());

        }

        return date;

    }


    public static String localToUTC(String date) {


        try {


            DateFormat localFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));


            date = utcFormat.format(localFormat.parse(date));

            Log.e("UtcDate", date);


        } catch (ParseException e) {
            Log.e("ParseExceptionDateConv", ">>" + e.getMessage());
        }

        return date;

    }

    public static String localToUTCSurvey(String date) {


        try {


            DateFormat localFormat = new SimpleDateFormat("dd-MM-yyyy");

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));


            date = utcFormat.format(localFormat.parse(date));

            Log.e("UtcDate", date);


        } catch (ParseException e) {
            Log.e("ParseExceptionDateConv", ">>" + e.getMessage());
        }

        return date;

    }
}
