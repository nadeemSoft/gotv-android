package com.app.gotv.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.app.gotv.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by ist on 23/8/17.
 */

public class AppUtil {


    private static int widthPixels = 0;

    public static boolean isEmailValid(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }

        /*String emailPattern ="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern)) {
            return true;
        }

        return false;*/
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    public static boolean isNetworkAvailable(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static Bitmap decodeBase64(String input) {
        try {
            byte[] decodedByte = Base64.decode(input, 0);
            return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
        } catch (Exception e) {
            return null;
        }
    }

    public static void hideSoftKeyboard(Context context) {
        Activity activity = (Activity) context;
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void hideSoftKeyboardDialog(Context context, Dialog dialog) {
        Activity activity = (Activity) context;
        if (dialog.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(dialog.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 1);

    }

    public static void shareIntent(Context context, String msg) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
//                sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out my app at: https://play.google.com/store/apps/details?id=com.google.android.apps.plus");
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        sendIntent.setType("text/plain");
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(sendIntent);
    }

    public static String createImageFromBitmap(Bitmap mBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/Gotv/Uploads");
        myDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        File file = new File(myDir, "IMG_" + timeStamp + ".jpeg");
        Log.e("myDir", myDir.toString());

        String currentTemplatePath = file.toString();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

            return currentTemplatePath;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    public static RelativeLayout.LayoutParams setImageSizeForPopularVIew(View img, Activity context, int widthRatio, int heightRatio) {
        Log.d("ratio", "width : " + widthRatio + " height " + heightRatio);
        int width = getDeviceWidth(context);
        Log.d("ratio", "screen width : " + width);

        if (widthRatio != 0) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, ((((width * heightRatio) / widthRatio) * 100) / 100) + 85);
            img.setLayoutParams(params);
            Log.d("ratio", "calculate width : " + width + " calculate height : " + (((width * heightRatio) / widthRatio) * 100) / 100);
            return params;
        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, (((width * 1) / 1) * 100) / 100);
            img.setLayoutParams(params);
            return params;
        }
    }

    public static RelativeLayout.LayoutParams setImageSizeForDescriptionView(View img, Activity context, int widthRatio, int heightRatio) {
        Log.d("ratio", "width : " + widthRatio + " height " + heightRatio);
        int width = getDeviceWidth(context);
        Log.d("ratio", "screen width : " + width);

        if (widthRatio != 0) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, ((((width * heightRatio) / widthRatio) * 100) / 100));
            img.setLayoutParams(params);
            Log.d("ratio", "calculate width : " + width + " calculate height : " + (((width * heightRatio) / widthRatio) * 100) / 100);
            return params;
        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, (((width * 1) / 1) * 100) / 100);
            img.setLayoutParams(params);
            return params;
        }
    }

    public static LinearLayout.LayoutParams setImageSizeForHomeView(View img, Activity context, int widthRatio, int heightRatio) {
        Log.d("ratio", "width : " + widthRatio + " height " + heightRatio);
        int width = getDeviceWidth(context);
        Log.d("ratio", "screen width : " + width);

        if (widthRatio != 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, ((((width * heightRatio) / widthRatio) * 100) / 100) + 85);
            img.setLayoutParams(params);
            Log.d("ratio", "calculate width : " + width + " calculate height : " + (((width * heightRatio) / widthRatio) * 100) / 100);
            return params;
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, (((width * 1) / 1) * 100) / 100);
            img.setLayoutParams(params);
            return params;
        }
    }


    public static int getDeviceWidth(Activity context) {
        if (widthPixels == 0) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            widthPixels = displayMetrics.widthPixels;
            Log.e("device width", widthPixels + "");
        }
        return widthPixels;
    }

    public static void showCloseAppDialog(final Activity activity) {
/**
 * Show Dialog....
 */
        final Dialog dialog1 = new Dialog(activity);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1.setContentView(R.layout.common_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog1.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog1.getWindow().setAttributes(lp);

        TextView contentText = dialog1.findViewById(R.id.popup_content);
        contentText.setText(activity.getResources().getString(R.string.close_app));
        TextView btnNo = dialog1.findViewById(R.id.popup_no_btn);
        TextView btnOk = dialog1.findViewById(R.id.popup_yes_btn);

        dialog1.setCancelable(true);

        try {
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog1.cancel();
                    activity.finish();
                }
            });

            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog1.dismiss();
                }
            });

        } catch (Exception e) {
        }


        dialog1.show();

    }


    public static String getDate(String date) {

        String finalDate = date;

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String pastDate = date;

            Log.e("LOCAL TIME>>", ">>" + pastDate);

            Date past = format.parse(pastDate);


        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ParseException", e.getMessage());
        }

        return finalDate;

    }


    public static void showCommonPopup(Context activity, String sms) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_popup);

        TextView tvMale = dialog.findViewById(R.id.popup_content);
        tvMale.setText("" + sms);
        TextView tvFemale = dialog.findViewById(R.id.popup_content_description);
        tvFemale.setText("" + activity.getResources().getString(R.string.ok));
        tvFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });


        dialog.show();

    }




    public static Dialog showLoaderDialog(Context mActivity, boolean isCancellable) {

        Dialog loadingDialog = new Dialog(mActivity);
        loadingDialog.setCancelable(isCancellable);
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.setContentView(R.layout.progress_hud);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loadingDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return loadingDialog;
    }





}
