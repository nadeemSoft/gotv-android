package com.app.gotv.sessionManagement;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

public class AppSession {
    private static AppSession appSession;
    private Context context;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "MyAppPreference";


    private AppSession(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.commit();
    }


    public static AppSession getInstance(Context context) {
        if (appSession == null) {

            appSession = new AppSession(context);
        }
        return appSession;
    }


    public void saveLanguage(String lang) {
        editor.putString(PreferenceKey.SAVE_LANGUAGE, lang);
        editor.commit();
    }

    public String getLanguage() {
        return pref.getString(PreferenceKey.SAVE_LANGUAGE, "en");

    }

    public void setUserLogin(boolean userLogin) {
        editor.putBoolean(PreferenceKey.SET_USER_LOGIN, userLogin);
        editor.commit();
    }

    public boolean getUserLogin() {
        return pref.getBoolean(PreferenceKey.SET_USER_LOGIN, false);


    }


    public String getAccessToken() {
        return pref.getString(PreferenceKey.PREFERENCE_KEY_ACCESS_TOKEN, null);

    }

    public String getRefreshToken() {
        return pref.getString(PreferenceKey.PREFERENCE_KEY_REFRESH_TOKEN, null);

    }


    public void setToken(String accesstoken, String refreshToken) {
        editor.putString(PreferenceKey.PREFERENCE_KEY_REFRESH_TOKEN, refreshToken);
        editor.putString(PreferenceKey.PREFERENCE_KEY_ACCESS_TOKEN, accesstoken);
        editor.commit();

    }

    public void setUserImage(String userImage) {
        editor.putString(PreferenceKey.KEY_USER_IMAGE, userImage);
        editor.commit();

    }

    public String getUserImage() {
        return pref.getString(PreferenceKey.KEY_USER_IMAGE, null);

    }


    public void setMyName(String name) {
        editor.putString(PreferenceKey.KEY_MY_NAME, name);
        editor.commit();
    }


    public String getMyName() {
        return pref.getString(PreferenceKey.KEY_MY_NAME, null);

    }


    public boolean logoutUser() {
        editor.clear();
        editor.commit();
        return true;
    }


    public void saveRegionData(JSONObject object) {
        editor.putString(PreferenceKey.KEY_SAVE_REGION_OBJECT, object.toString());
        editor.commit();
    }


    public String getRegionData() {
        return pref.getString(PreferenceKey.KEY_SAVE_REGION_OBJECT, "");

    }


    public void saveElectorailData(JSONObject object) {
        editor.putString(PreferenceKey.KEY_SAVE_ELECTORIAL_OBJECT, object.toString());
        editor.commit();
    }


    public String getElectorailData() {
        return pref.getString(PreferenceKey.KEY_SAVE_ELECTORIAL_OBJECT, "");

    }


    public void saveBranchData(JSONObject object) {
        editor.putString(PreferenceKey.KEY_SAVE_BRANCH_OBJECT, object.toString());
        editor.commit();
    }


    public String getBranchData() {
        return pref.getString(PreferenceKey.KEY_SAVE_BRANCH_OBJECT, "");

    }

    public void saveConstituencyData(JSONObject object) {
        editor.putString(PreferenceKey.KEY_SAVE_CONSTITUENCY_OBJECT, object.toString());
        editor.commit();
    }

    public String getConstituencyData() {
        return pref.getString(PreferenceKey.KEY_SAVE_CONSTITUENCY_OBJECT, "");

    }




}
