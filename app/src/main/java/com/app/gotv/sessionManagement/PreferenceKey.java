package com.app.gotv.sessionManagement;

public class PreferenceKey {
    public static final String SAVE_LANGUAGE = "lang";
    public static final String SET_USER_LOGIN = "login";
    public static final String PREFERENCE_KEY_ACCESS_TOKEN = "access_token";
    public static final String PREFERENCE_KEY_REFRESH_TOKEN = "refresh_token";
    public static final String KEY_TOKEN_TYPE = "token_type";
    public static final String KEY_USER_IMAGE = "imageUrl";
    public static final String SAVE_FIRE_BASE_FCM_TOKEN = "fcmToken";
    public static final String KEY_MY_NAME = "myName";
    public static final String KEY_SAVE_ALL_CATEGORY = "saveAllCategory";
    public static final String KEY_SAVE_ALL_CATEGORY_DATE = "saveAllCategoryData";
    public static final String KEY_SAVE_REGION_OBJECT = "region";
    public static final String KEY_SAVE_CONSTITUENCY_OBJECT = "constituency";
    public static final String KEY_SAVE_ELECTORIAL_OBJECT = "electorial";
    public static final String KEY_SAVE_BRANCH_OBJECT = "branch";


}
